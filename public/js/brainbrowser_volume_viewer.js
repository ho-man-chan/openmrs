BrainBrowser.VolumeViewer.start("brainbrowser", function(viewer) {
  
  // Add an event listener.
  viewer.addEventListener("volumesloaded", function() {
    console.log("Viewer is ready!");
    console.log("event listener here....");
  });

  // Load the default color map.
  // (Second argument is the cursor color to use).
  viewer.loadDefaultColorMapFromURL("/color-maps/spectral-brainview.txt", "#FF0000");

  // Set the size of slice display panels.
  viewer.setPanelSize(256, 256);

  // Start rendering.
  viewer.render();

  // Load volumes.
  viewer.loadVolumes({
    volumes: [
      {
        type: "minc",
        header_url: "/models/functional.mnc.header",
        raw_data_url: "/models/functional.mnc.raw",
        template: {
          element_id: "volume-ui-template",
          viewer_insert_class: "volume-viewer-display"
        }
      },
    ],
    overlay: {
      template: {
        element_id: "overlay-ui-template",
        viewer_insert_class: "overlay-viewer-display"
      }
    }
  });
});