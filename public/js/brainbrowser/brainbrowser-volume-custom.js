BrainBrowser.VolumeViewer.start("brainbrowser", function(viewer) {
  
   // Add an event listener.
   viewer.addEventListener("volumesloaded", function() {
     console.log("Viewer is ready!");
     console.log("event listener here....");
   });

   // Load the default color map.
   // (Second argument is the cursor color to use).
   viewer.loadDefaultColorMapFromURL("/vendor/brainbrowser/color-maps/gray-scale.txt", "#FF0000");

   // Set the size of slice display panels.
   viewer.setPanelSize(256, 256);

   // Start rendering.
   viewer.render();

   // Load volumes.
  //  viewer.loadVolumes({
  //    volumes: [
  //      {
  //        type: "minc",
  //        header_url: "/vendor/brainbrowser/models/functional.mnc.header",
  //        raw_data_url: "/vendor/brainbrowser/models/functional.mnc.raw",
  //        template: {
  //          element_id: "volume-ui-template",
  //          viewer_insert_class: "volume-viewer-display"
  //        }
  //       },
  //       {
  //         type: "nifti1",
  //         nii_url: "/vendor/brainbrowser/models/functional.nii",
  //         template: {
  //           element_id: "volume-ui-template",
  //           viewer_insert_class: "volume-viewer-display"
  //         }
  //       },
  //    ],
  //    overlay: {
  //      template: {
  //        element_id: "overlay-ui-template",
  //        viewer_insert_class: "overlay-viewer-display"
  //      }
  //    }
  //  });
  viewer.volumes.forEach(function(volume) {
    volume.display.forEach(function(panel) {
      panel.addEventListener('cursorupdate', function(e) {
        console.log('helooooo')
      })
    })
  })

  let showDivs = function(n, slideIndex) {
    let i;
    let x = document.getElementsByClassName("slice-display");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";  
    }
    x[slideIndex-1].style.display = "block";
  }

  let plusDivs = function(n, slideIndex) {
    showDivs(slideIndex += n);
  }

  document.getElementById("panel-size").onchange = function(e) {
    let volViewerDisplayEl = document.getElementById("panel-size");
  }

  document.getElementById("v-pills-tab").onclick = function(e) {
    viewer.clearVolumes();
    viewer.loadVolume({
      type: "nifti1",
      nii_url: e.target.attributes.value.nodeValue,
      template: {
        element_id: "volume-ui-template",
        viewer_insert_class: "volume-viewer-display"
      }
    }, function() {
        viewer.volumes.forEach(function(volume) {
          volume.display.forEach(function(panel) {
            panel.addEventListener('cursorupdate', function(e) {
              panel.cursor = {x: e.cursor.x, y: e.cursor.y};
            })
          })
        })
        let sizeEl = document.getElementById('panel-size');
        let volumeViewerDisplayEl = document.getElementsByClassName('volume-viewer-display');
        let sliceDisplayEl = document.getElementsByClassName('slice-display');
        document.getElementsByClassName('volume-container')[0].style.display= 'flex';
        volumeViewerDisplayEl[0].style.display = 'flex';
        volumeViewerDisplayEl[0].style.width = '100%';
        volumeViewerDisplayEl[0].style.position = 'relative';

        let sagittal = document.createElement('span');
        let coronal = document.createElement('span');
        let transverse = document.createElement('span');

        if (sizeEl[sizeEl.selectedIndex].value == 300) {
          for (let i = 0; i < sliceDisplayEl.length; i++) {
            viewer.setPanelSize(300, 300, { scale_image: true });
            // sliceDisplayEl[i].style.width = '300px';
            sliceDisplayEl[i].style.height = '300px';
          }
        } else if (sizeEl[sizeEl.selectedIndex].value == 500) {
          volumeViewerDisplayEl[0].style.flexDirection = 'column';
          volumeViewerDisplayEl[0].style.alignItems = 'center';
          for (let i = 0; i < sliceDisplayEl.length; i++) {
            viewer.setPanelSize(500, 500, { scale_image: true });
            // sliceDisplayEl[i].style.width = '500px';
            // sliceDisplayEl[i].style.height = '500px';
          }
        } else {
          let slideIndex = 1;
          let leftArrow = document.createElement('span');
          leftArrow.setAttribute('id', 'leftArrow');
          leftArrow.setAttribute('class', 'glyphicon glyphicon-chevron-left');
          // leftArrow.setAttribute('aria-hidden', 'true');
          let leftDiv = document.createElement('button');
          leftDiv.setAttribute('type', 'button');
          leftDiv.setAttribute('class', 'btn btn-default');
          leftDiv.appendChild(leftArrow);
          volumeViewerDisplayEl[0].appendChild(leftDiv);
          let rightArrow = document.createElement('span');
          rightArrow.setAttribute('id', 'rightArrow');
          rightArrow.setAttribute('class', 'glyphicon glyphicon-chevron-right');
          // rightArrow.setAttribute('aria-hidden', 'true');
          let rightDiv = document.createElement('button');
          rightDiv.setAttribute('type', 'button');
          rightDiv.setAttribute('class', 'btn btn-default');
          rightDiv.appendChild(rightArrow);
          volumeViewerDisplayEl[0].appendChild(rightDiv);

          volumeViewerDisplayEl[0].style.justifyContent = 'center';
          viewer.setPanelSize(700, 700, { scale_image: true });
          for (let i = 0; i < sliceDisplayEl.length; i++) {
            sliceDisplayEl[i].style.width = '700px';
            sliceDisplayEl[i].style.height = '700px';
          }

          let plusDivs = function(n, sliceDisplayEl) {
            showDivs(slideIndex += n, sliceDisplayEl);
          }

          let showDivs = function(n,sliceDisplayEl) {
            let i;
            if (n > sliceDisplayEl.length) {slideIndex = 1}
            if (n < 1) {slideIndex = sliceDisplayEl.length}
            for (i = 0; i < sliceDisplayEl.length; i++) {
              sliceDisplayEl[i].style.display = 'none';
              if (i == 0) {
                sagittal.style.display = 'none';
              } else if (i == 1) {
                coronal.style.display = 'none';
              } else {
                transverse.style.display = 'none';
              }
            }
            sliceDisplayEl[slideIndex-1].style.display = 'block';
            if (slideIndex - 1 == 0) {
              sagittal.style.display = 'block';
            } else if (slideIndex - 1 == 1) {
              coronal.style.display = 'block';
            } else {
              transverse.style.display = 'block';
            }
          }
          showDivs(slideIndex, sliceDisplayEl);
        }

        let text1 = document.createTextNode('Sagittal');
        sagittal.appendChild(text1);
        sagittal.style.position = 'absolute';
        sagittal.style.color = 'white';
        sagittal.style.top = sliceDisplayEl[0].offsetTop.toString().concat('px');
        sagittal.style.left = sliceDisplayEl[0].offsetLeft.toString().concat('px');
        volumeViewerDisplayEl[0].appendChild(sagittal);

        let text2 = document.createTextNode('Coronal');
        coronal.appendChild(text2);
        coronal.style.position = 'absolute';
        coronal.style.top = sliceDisplayEl[1].offsetTop.toString().concat('px');
        coronal.style.left = sliceDisplayEl[1].offsetLeft.toString().concat('px');
        volumeViewerDisplayEl[0].appendChild(coronal);
        
        let text3 = document.createTextNode('Transverse');
        transverse.appendChild(text3);
        transverse.style.position = 'absolute';
        transverse.style.top = sliceDisplayEl[2].offsetTop.toString().concat('px');
        transverse.style.left = sliceDisplayEl[2].offsetLeft.toString().concat('px');
        volumeViewerDisplayEl[0].appendChild(transverse);
        // document.getElementsByClassName("slice-display")[0].textContent = 'Sagittal';
        // let firstCanvas = document.getElementsByClassName("slice-display")[0];
        // let firstSlice = firstCanvas.getContext("2d");
        // firstSlice.fillStyle = 'white';
        // firstSlice.font = "bold 16px Arial";
        // firstSlice.textAlign = 'center';
        // firstSlice.textBaseline = 'middle';
        // firstSlice.fillText('Sagittal', firstCanvas.width / 2, firstCanvas.height / 6);
        // let secondCanvas = document.getElementsByClassName("slice-display")[1];
        // let secondSlice = secondCanvas.getContext("2d");
        // secondSlice.fillStyle = 'white';
        // secondSlice.font = "bold 16px Arial";
        // secondSlice.textAlign = 'center';
        // secondSlice.textBaseline = 'middle';
        // secondSlice.fillText('Coronal', secondCanvas.width / 2, secondCanvas.height / 6);
        // let thirdCanvas = document.getElementsByClassName("slice-display")[2]
        // let thirdSlice = thirdCanvas.getContext("2d");
        // thirdSlice.fillStyle = 'white';
        // thirdSlice.font = "bold 16px Arial";
        // thirdSlice.textAlign = 'center';
        // thirdSlice.textBaseline = 'middle';
        // thirdSlice.fillText('Transverse', thirdCanvas.width / 2, thirdCanvas.height / 6);
    })
  }

  // var button = document.getElementById("volume-file-nifti1-submit");
  // button.onclick = function() {
  //   let checkboxParent = document.getElementById("nifti-checkbox");
  //   let checkedNiftiFiles = []
  //   checkedNiftiFiles.forEach.call(checkboxParent.children,function(child, index) {
  //     if(child.checked) {
  //       checkedNiftiFiles.push({
  //         type: "nifti1",
  //         nii_url: child.value,
  //         template: {
  //           element_id: "volume-ui-template",
  //           viewer_insert_class: "volume-viewer-display"
  //         }
  //       })
  //     }
  //   })
  //   viewer.clearVolumes();
  //   viewer.loadVolumes({
  //     volumes: checkedNiftiFiles,
  //   });
  // }
  // document.getElementById("volume-file").style.display = 'block';
  // var button = document.getElementById("volume-file-nifti1-submit");
  // button.onclick = function() {
  //   viewer.clearVolumes();
  //   viewer.loadVolume({
  //     type: "nifti1",
  //     nii_file: document.getElementById("nifti1-file"),
  //     template: {
  //       element_id: "volume-ui-template",
  //       viewer_insert_class: "volume-viewer-display"
  //     }
  //   }, function() {
  //     document.getElementsByClassName("volume-viewer-display")[0].style.display = 'flex';
  //     document.getElementsByClassName("slice-display")[0].style.display = 'inline';
  //     document.getElementsByClassName("volume-controls")[0].style = 'width: auto; display: none;';
  //     console.log(document.getElementById("nifti1-file").value);
  //   });
  // }

  // viewer.addEventListener("volumeuiloaded", function(event) {
  //   var container = event.container;
  //   var volume = event.volume;
  //   var vol_id = event.volume_id;
  // 
  //   var x = parseFloat(container.querySelector("#world-x-").value);
  //   var y = parseFloat(container.querySelector("#world-y-").value);
  //   var z = parseFloat(container.querySelector("#world-z-").value);
  // 
  //   if(!BrainBrowser.utils.isNumeric(x)) {
  //     x = 0;
  //   }
  // 
  //     // Make sure the values are numeric.
  // 
  //   if(!BrainBrowser.utils.isNumeric(y)) {
  //     y = 0;
  //   }
  // 
  //   if(!BrainBrowser.utils.isNumeric(y)) {
  //     z = 0;
  //   }
  // 
  //   if(viewer.synced) {
  //     viewer.volumes.forEach(function(volume) {
  //       volume.setWorldCoords(x, y, z);
  //     });
  //   } else {
  //     viewer.volumes[vol_id].setWorldCoords(x, y, z);
  //   }
  //   The world coordinate input fields.
  //   container.querySelector('.world-coords').addEventListener('change', function() {
  //     console.log(this.value);
  //     var x = parseFloat(this.value.getElementById("world-x-" + vol_id).value);
  //     var y = parseFloat(this.value.getElementById("world-y-" + vol_id).value);
  //     var z = parseFloat(this.value.getElementById("world-z-" + vol_id).value);
  // 
  //     // Make sure the values are numeric.
  //     if (!BrainBrowser.utils.isNumeric(x)) {
  //       x = 0;
  //     }
  //     if (!BrainBrowser.utils.isNumeric(y)) {
  //       y = 0;
  //     }
  //     if (!BrainBrowser.utils.isNumeric(z)) {
  //       z = 0;
  //     }
  // 
  //     // Set coordinates and redraw.
  //     if (viewer.synced) {
  //       viewer.volumes.forEach(function(volume) {
  //         volume.setWorldCoords(x, y, z);
  //       });
  //     }
  //     else {
  //       viewer.volumes[vol_id].setWorldCoords(x, y, z);
  //     }
  // 
  //     viewer.redrawVolumes();
  //   });
  // })
  // 
  // //////////////////////////////////
  // Per volume UI hooks go in here.
  //////////////////////////////////
  viewer.addEventListener("volumeuiloaded", function(event) {
    var container = event.container;
    var volume = event.volume;
    var vol_id = event.volume_id;

    container = $(container);

    container.find(".button").button();

    // The world coordinate input fields.
    container.find(".world-coords").change(function() {
      var div = $(this);

      var x = parseFloat(div.find("#world-x-" + vol_id).val());
      var y = parseFloat(div.find("#world-y-" + vol_id).val());
      var z = parseFloat(div.find("#world-z-" + vol_id).val());

      // Make sure the values are numeric.
      if (!BrainBrowser.utils.isNumeric(x)) {
        x = 0;
      }
      if (!BrainBrowser.utils.isNumeric(y)) {
        y = 0;
      }
      if (!BrainBrowser.utils.isNumeric(z)) {
        z = 0;
      }

      // Set coordinates and redraw.
      if (viewer.synced) {
        viewer.volumes.forEach(function(volume) {
          volume.setWorldCoords(x, y, z);
        });
      }
      else {
        viewer.volumes[vol_id].setWorldCoords(x, y, z);
      }

      viewer.redrawVolumes();
    });

    // The world coordinate input fields.
    container.find(".voxel-coords").change(function() {
      var div = $(this);

      var i = parseInt(div.find("#voxel-i-" + vol_id).val(), 10);
      var j = parseInt(div.find("#voxel-j-" + vol_id).val(), 10);
      var k = parseInt(div.find("#voxel-k-" + vol_id).val(), 10);

      // Make sure the values are numeric.
      if (!BrainBrowser.utils.isNumeric(i)) {
        i = 0;
      }
      if (!BrainBrowser.utils.isNumeric(j)) {
        j = 0;
      }
      if (!BrainBrowser.utils.isNumeric(k)) {
        k = 0;
      }

      // Set coordinates and redraw.
      viewer.volumes[vol_id].setVoxelCoords(i, j, k);
      if (viewer.synced) {
        var synced_volume = viewer.volumes[vol_id];
        var wc = synced_volume.getWorldCoords();
        viewer.volumes.forEach(function(volume) {
          if (synced_volume !== volume) {
            volume.setWorldCoords(wc.x, wc.y, wc.z);
          }
        });
      }

      viewer.redrawVolumes();
    });

    // Color map URLs are read from the config file and added to the
    // color map select box.
    var color_map_select = $('<select id="color-map-select"></select>').change(function() {
      var selection = $(this).find(":selected");

      viewer.loadVolumeColorMapFromURL(vol_id, selection.val(), selection.data("cursor-color"), function() {
        viewer.redrawVolumes();
      });
    });

    BrainBrowser.config.get("color_maps").forEach(function(color_map) {
      color_map_select.append('<option value="' + color_map.url +
        '" data-cursor-color="' + color_map.cursor_color + '">' +
        color_map.name +'</option>'
      );
    });

    $("#color-map-" + vol_id).append(color_map_select);

    // Load a color map select by the user.
    container.find(".color-map-file").change(function() {
      viewer.loadVolumeColorMapFromFile(vol_id, this, "#FF0000", function() {
        viewer.redrawVolumes();
      });
    });

    // Change the range of intensities that will be displayed.
    container.find(".threshold-div").each(function() {
      var div = $(this);

      // Input fields to input min and max thresholds directly.
      var min_input = div.find("#min-threshold-" + vol_id);
      var max_input = div.find("#max-threshold-" + vol_id);

      // Slider to modify min and max thresholds.
      var slider = div.find(".slider");

      var volume = viewer.volumes[vol_id];

      // Update the input fields.
      min_input.val(volume.getVoxelMin());
      max_input.val(volume.getVoxelMax());

      slider.slider({
        range: true,
        min: volume.getVoxelMin(),
        max: volume.getVoxelMax(),
        values: [volume.getVoxelMin(), volume.getVoxelMax()],
        step: 1,
        slide: function(event, ui){
          var values = ui.values;

          // Update the input fields.
          min_input.val(values[0]);
          max_input.val(values[1]);

          // Update the volume and redraw.
          volume.intensity_min = values[0];
          volume.intensity_max = values[1];
          viewer.redrawVolumes();
        },
        stop: function() {
          $(this).find("a").blur();
        }
      });

      // Input field for minimum threshold.
      min_input.change(function() {
        var value = parseFloat(this.value);

        // Make sure input is numeric and in range.
        if (!BrainBrowser.utils.isNumeric(value)) {
          value = volume.getVoxelMin();
        }
        value = Math.max(volume.getVoxelMin(),
                         Math.min(value, volume.getVoxelMax()));
        this.value = value;

        // Update the slider.
        slider.slider("values", 0, value);

        // Update the volume and redraw.
        volume.intensity_min = value;
        viewer.redrawVolumes();
      });

      max_input.change(function() {
        var value = parseFloat(this.value);

        // Make sure input is numeric and in range.
        if (!BrainBrowser.utils.isNumeric(value)) {
          value = volume.getVoxelMax();
        }
        value = Math.max(volume.getVoxelMin(),
                         Math.min(value, volume.getVoxelMax()));
        this.value = value;

        // Update the slider.
        slider.slider("values", 1, value);

        // Update the volume and redraw.
        volume.intensity_max = value;
        viewer.redrawVolumes();
      });

    });

    container.find(".time-div").each(function() {
      var div = $(this);

      if (volume.header.time) {
        div.show();
      } else {
        return;
      }

      var slider = div.find(".slider");
      var time_input = div.find("#time-val-" + vol_id);
      var play_button = div.find("#play-" + vol_id);

      var min = 0;
      var max = volume.header.time.space_length - 1;
      var play_interval;

      slider.slider({
        min: min,
        max: max,
        value: 0,
        step: 1,
        slide: function(event, ui) {
          var value = +ui.value;
          time_input.val(value);
          volume.current_time = value;
          viewer.redrawVolumes();
        },
        stop: function() {
          $(this).find("a").blur();
        }
      });

      time_input.change(function() {
        var value = parseInt(this.value, 10);
        if (!BrainBrowser.utils.isNumeric(value)) {
          value = 0;
        }

        value = Math.max(min, Math.min(value, max));

        this.value = value;
        time_input.val(value);
        slider.slider("value", value);
        volume.current_time = value;
        viewer.redrawVolumes();
      });

      play_button.change(function() {
        if(play_button.is(":checked")){
          clearInterval(play_interval);
          play_interval = setInterval(function() {
            var value = volume.current_time + 1;
            value = value > max ? 0 : value;
            volume.current_time = value;
            time_input.val(value);
            slider.slider("value", value);
            viewer.redrawVolumes();
          }, 200);
        } else {
          clearInterval(play_interval);
        }
      });

    });

    // Create an image of all slices in a certain
    // orientation.
    container.find(".slice-series-div").each(function() {
      var div = $(this);

      var space_names = {
        xspace: "Sagittal",
        yspace: "Coronal",
        zspace: "Transverse"
      };

      div.find(".slice-series-button").click(function() {
        var axis_name = $(this).data("axis");
        var axis = volume.header[axis_name];
        var space_length = axis.space_length;
        var time = volume.current_time;
        var per_column = 10;
        var zoom = 0.5;
        var i, x, y;

        // Canvas on which to draw the images.
        var canvas = document.createElement("canvas");
        var context = canvas.getContext("2d");

        // Get first slice to set dimensions of the canvas.
        var image_data = volume.getSliceImage(volume.slice(axis_name, 0, time), zoom);
        var img = new Image();
        canvas.width = per_column * image_data.width;
        canvas.height = Math.ceil(space_length / per_column) * image_data.height;
        context.fillStyle = "#000000";
        context.fillRect(0, 0, canvas.width, canvas.height);

        // Draw the slice on the canvas.
        context.putImageData(image_data, 0, 0);

        // Draw the rest of the slices on the canvas.
        for (i = 1; i < space_length; i++) {
          image_data = volume.getSliceImage(volume.slice(axis_name, i, time), zoom);
          x = i % per_column * image_data.width;
          y = Math.floor(i / per_column) * image_data.height;
          context.putImageData(image_data, x, y);
        }

        // Retrieve image from canvas and display it
        // in a dialog box.
        img.onload = function() {
          $("<div></div>").append(img).dialog({
            title: space_names[axis_name] + " Slices",
            height: 600,
            width: img.width
          });
        };

        img.src = canvas.toDataURL();
      });
    });

    // Blend controls for a multivolume overlay.
    container.find(".blend-div").each(function() {
      var div = $(this);
      var slider = div.find(".slider");
      var blend_input = div.find("#blend-val");

      // Slider to select blend value.
      slider.slider({
        min: 0,
        max: 1,
        step: 0.01,
        value: 0.5,
        slide: function(event, ui) {
          var value = parseFloat(ui.value);
          volume.blend_ratios[0] = 1 - value;
          volume.blend_ratios[1] = value;



          blend_input.val(value);
          viewer.redrawVolumes();
        },
        stop: function() {
          $(this).find("a").blur();
        }
      });

      // Input field to select blend values explicitly.
      blend_input.change(function() {
        var value = parseFloat(this.value);

        // Check that input is numeric and in range.
        if (!BrainBrowser.utils.isNumeric(value)) {
          value = 0;
        }
        value = Math.max(0, Math.min(value, 1));
        this.value = value;

        // Update slider and redraw volumes.
        slider.slider("value", value);
        volume.blend_ratios[0] = 1 - value;
        volume.blend_ratios[1] = value;
        viewer.redrawVolumes();
      });
    });

    // Contrast controls
    container.find(".contrast-div").each(function() {
      var div = $(this);
      var slider = div.find(".slider");
      var contrast_input = div.find("#contrast-val");

      // Slider to select contrast value.
      slider.slider({
        min: 0,
        max: 2,
        step: 0.05,
        value: 1,
        slide: function(event, ui) {
          var value = parseFloat(ui.value);
          volume.display.setContrast(value);
          volume.display.refreshPanels();

          contrast_input.val(value);
        },
        stop: function() {
          $(this).find("a").blur();
        }
      });

      // Input field to select contrast values explicitly.
      contrast_input.change(function() {
        var value = parseFloat(this.value);

        // Check that input is numeric and in range.
        if (!BrainBrowser.utils.isNumeric(value)) {
          value = 0;
        }
        value = Math.max(0, Math.min(value, 2));
        this.value = value;

        // Update slider and redraw volumes.
        slider.slider("value", value);
        volume.display.setContrast(value);
        volume.display.refreshPanels();
        viewer.redrawVolumes();
      });
    });

    // Contrast controls
    container.find(".brightness-div").each(function() {
      var div = $(this);
      var slider = div.find(".slider");
      var brightness_input = div.find("#brightness-val");

      // Slider to select brightness value.
      slider.slider({
        min: -1,
        max: 1,
        step: 0.05,
        value: 0,
        slide: function(event, ui) {
          var value = parseFloat(ui.value);
          volume.display.setBrightness(value);
          volume.display.refreshPanels();

          brightness_input.val(value);
        },
        stop: function() {
          $(this).find("a").blur();
        }
      });

      // Input field to select brightness values explicitly.
      brightness_input.change(function() {
        var value = parseFloat(this.value);

        // Check that input is numeric and in range.
        if (!BrainBrowser.utils.isNumeric(value)) {
          value = 0;
        }
        value = Math.max(-1, Math.min(value, 1));
        this.value = value;

        // Update slider and redraw volumes.
        slider.slider("value", value);
        volume.display.setBrightness(value);
        volume.display.refreshPanels();
        viewer.redrawVolumes();
      });
    });
  });
  
  /* This function simply takes an input hex background color
   * and returns either "black" or "white" as the appropriate
   * foreground color for text rendered over the background colour.
   * Idea from https://24ways.org/2010/calculating-color-contrast/
   * Equation is from http://www.w3.org/TR/AERT#color-contrast
   */
  function getContrastYIQ(hexcolor) {
    var r = parseInt(hexcolor.substr(0, 2), 16);
    var g = parseInt(hexcolor.substr(2, 2), 16);
    var b = parseInt(hexcolor.substr(4, 2), 16);
    var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
    return (yiq >= 128) ? 'black' : 'white';
  }
  
  viewer.addEventListener("sliceupdate", function(event) {
    var panel = event.target;
    var volume = event.volume;
    var vol_id = panel.volume_id;
    var world_coords, voxel_coords;
    var value;

    if (BrainBrowser.utils.isFunction(volume.getWorldCoords)) {
      world_coords = volume.getWorldCoords();
      $("#world-x-" + vol_id).val(world_coords.x.toPrecision(6));
      $("#world-y-" + vol_id).val(world_coords.y.toPrecision(6));
      $("#world-z-" + vol_id).val(world_coords.z.toPrecision(6));
    }

    if (BrainBrowser.utils.isFunction(volume.getVoxelCoords)) {
      voxel_coords = volume.getVoxelCoords();
      $("#voxel-i-" + vol_id).val(parseInt(voxel_coords.i, 10));
      $("#voxel-j-" + vol_id).val(parseInt(voxel_coords.j, 10));
      $("#voxel-k-" + vol_id).val(parseInt(voxel_coords.k, 10));
    }

    value = volume.getIntensityValue();

    /* Set background color of intensity value to match colormap
     * entry for that value.
     */
    var bg_color = volume.color_map.colorFromValue(value, {
      hex: true,
      min: volume.intensity_min,
      max: volume.intensity_max,
      contrast: panel.contrast,
      brightness: panel.brightness
    });

    /* Given that the background color has a wide range, use a little
     * cleverness to pick either white or black as the foreground color
     * of the intensity value. This improves readability.
     */
    var fg_color = getContrastYIQ(bg_color);

    $("#intensity-value-" + vol_id)
    .css("background-color", "#" + bg_color)
    .css("color", fg_color)
    .html(Math.floor(value));

    if (volume.header && volume.header.time) {
      $("#time-slider-" + vol_id).slider("option", "value", volume.current_time);
      $("#time-val-" + vol_id).val(volume.current_time);
    }
  });
});

