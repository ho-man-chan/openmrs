<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //create admin user account
      DB::table('users')->insert([
          'firstName' => env('USER_FIRSTNAME'),
          'lastName' => env('USER_LASTNAME'),
          'username' => env('USER_USERNAME'),
          'email' => env('USER_EMAIL'),
          'password' => bcrypt(env('USER_PASS')),
      ]);

      //create admin role
      $role = Role::create(['name' => 'admin']);
      //find admin user account by email
      $user = User::where('email', env('USER_EMAIL'))->first();
      //assign admin account with admin role
      $user->assignRole('admin');
    }
}
