<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('study_id');
            $table->string('seq');
            $table->string('samplingFreq')->default('5000');
            $table->string('transmitterFreq')->default('123e6');
            $table->string('echoTime')->default('155');
            $table->string('bfield')->default('3.0');
            $table->string('path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basis');
    }
}
