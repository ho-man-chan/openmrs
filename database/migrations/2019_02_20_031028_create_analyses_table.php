<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analyses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type')->nullable();
            $table->string('basis_path')->nullable();
            $table->integer('session_id');
            $table->string('sampling_freq');
            $table->string('transmitter_freq');
            $table->string('echo_time');
            $table->string('start_point')->default('15');
            $table->boolean('eddy_correction')->default(false);
            $table->boolean('stack_pdf')->default(true);
            $table->timestamps();
            $table->boolean('default');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analyses');
    }
}
