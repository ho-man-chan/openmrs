<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalysisResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analysis_results', function (Blueprint $table) {
          $table->increments('id');
          $table->string('path');
          $table->integer('analysis_id');
          $table->enum('type', ['megapress_results', 'press_results', 'megapress_graphs', 'press_graphs']);
          $table->enum('status',['waiting','preprocessing','preprocessed','analyzing','analyzed','error'])->default('waiting');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analysis_results');
    }
}
