<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('path');
            $table->integer('study_id')->nullable();
            $table->integer('session_id')->unsigned()->nullable();
            $table->foreign('session_id')->references('id')->on('sessions');
            
            // $table->integer('filable_id')->nullable();
            // $table->integer('filable_type')->nullable();

            $table->enum('type', [
              'basis',
              'megapress',
              'press',
              'megapress_lcm',
              'press_lcm',
              'megapress_preprocessed_report_pdf',
              'press_preprocessed_report_pdf',
              'megapress_results',
              'press_results',
              'megapress_graphs',
              'press_graphs',
              'dicoms',
              'dicoms_output',
              'nifti_output'
            ]);
            
           $table->enum('status',[
             'waiting',
             'preprocessing',
             'preprocessed',
             'analyzing',
             'analyzed',
             'error',
             'generated'
             ])->default('waiting');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
