@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <b>Create a Study</b>
    </div>

    <div class="card-body">
        <form method="POST" action="{{route('studies.basis')}}">
            @csrf
            <div class="form-group row">
                <div class="col">
                    <label for="studyName">Name of Study</label>
                    <input type="text" class="form-control" id="studyName" name="studyName" placeholder="E.g. Alzheimer Study">
                </div>
            </div>
            <div class="form-group row">
                <div class="col">
                    <label for="studyType">Study Type</label>
                    <input type="text" class="form-control" id="studyType" name="studyType" placeholder="E.g. Alzheimer">
                </div>
            </div>
            
            <button type="submit" name="button" value="basis" class="btn btn-success">Basis Set Options</button>
            <button type="submit" name="button" value ="create" class="btn btn-primary" dusk="study-create-button">Create Study</button>

        </form>

    </div>
</div>
@endsection