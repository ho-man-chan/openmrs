@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <b>Basis Set Options</b>
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route('studies.basisFIDA', [$study]) }}" enctype="multipart/form-data">
            @csrf

            @foreach($study->basis as $basis)
            <label>{{$basis->seq}} Basis Simulation</label>
            <div class="form-group row">
                <div class="col">
                    <label for="samplingFrequency">Sampling Frequency (Hz)</label>
                    <input type="text" class="form-control" id="samplingFrequency[]" name="samplingFrequency[]" value={{ $basis->samplingFreq }}>
                </div>
                <div class="col">
                    <label for="transmitterFrequency"> Transmitter Frequency (Hz)</label>
                    <input type="text" class="form-control" id="transmitterFrequency[]" name="transmitterFrequency[]" value={{ $basis->transmitterFreq }}>
                </div>

            </div>
            
            <div class="form-group row">
                <div class="col">
                    <label for="magneticFieldStrength">Magnetic Field Strength (T)</label>
                    <input type="text" class="form-control" id="magneticFieldStrength[]" name="magneticFieldStrength[]" value={{ $basis->bfield }}>
                </div>
                <div class="col">
                    <label for="echoTime">Echo Time (ms)</label>
                    <input type="text" class="form-control" id="echoTime[]" name="echoTime[]" value={{ $basis->echoTime }}>
                </div>

            </div>
            @endforeach

    
    <button type="submit" name="button" value="create" class="btn btn-primary">Create Study</button>

    </form>
</div>
</div>
@endsection