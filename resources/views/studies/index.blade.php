@extends('layouts.app')

@section('content')
  <div class="card">
    <div class="card-header">
      Studies
    </div>
    <div class="card-body">
      <div class="col-12 mb-2">
        <a
          href="{{route('studies.create')}}"
          class="btn btn-primary btn-sm"
          dusk="study-add-button"
        >
          Add Study
        </a>
      </div>
      <div class="col-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Type</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($studies as $study)
              <tr>
                <td>{{$study->id}}</td>
                <td>{{$study->name}}</td>
                <td>{{$study->type}}</td>
                <td>
                  <a href="{{route('sessions.index',['study'=>$study->id])}}" class="btn btn-light btn-sm">
                    <i class="fas fa-eye"></i> View
                  </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
