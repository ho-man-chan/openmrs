@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <b>Basis Set Options</b>
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route('studies.basisOptions', [$study]) }}" enctype="multipart/form-data">
            @csrf

            <div class="form-group row">
                <div class="col">
                    <label for="studyName">Name of Study</label>
                    <input type="text" class="form-control" id="studyName" name="studyName" placeholder="E.g. Alzheimer Study" value={{$study->name}} >
                </div>
            </div>
            <div class="form-group row">
                <div class="col">
                    <label for="studyType">Study Type</label>
                    <input type="text" class="form-control" id="studyType" name="studyType" placeholder="E.g. Alzheimer" value={{$study->type}} >
                </div>
            </div>

            <div class="form-group row">
                <div class="col">
                    <label for="sel1">Basis Simulation Method:</label>
                    <select class="form-control" id="method" name="method">
                        <option>TARQUIN</option>
                        <!-- <option>FID-A</option> -->
                        <option>Upload</option>
                    </select>
                </div>
            </div>

            <div class="form-check row">
            <label for="studyType">Add sequences to your study</label>
                <div class="col">
                    <input type="checkbox" class="form-check-input" id="press" name="press">
                    <label class="form-check-label" for="press">PRESS</label>
                </div>
                <div class="col">
                    <input type="checkbox" class="form-check-input" id="megapress" name="megapress">
                    <label class="form-check-label" for="megapress">MEGAPRESS</label>
                </div>
            </div>
            

            <button type="submit" name="button" value="create" class="btn btn-primary">Create Study</button>

        </form>

    </div>
</div>
@endsection