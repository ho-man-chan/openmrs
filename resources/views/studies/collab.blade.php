@extends('layouts.app')

@section('content')
<div class="container mb-2">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <b>Find the person to share</b>
                </div>

                <div class="card-body">
                    <div class="container">
                      <form class="form-horizontal" method="POST" action="{{ route('studies.collab.search.user', $study) }}">
                          {{ csrf_field() }}

                          <div class="form-group{{ $errors->has('people') ? ' has-error' : '' }}">
                              <label for="search" class="col-md-4 control-label">People</label>

                              <div class="col-md-12">
                                  <input pattern=".{2,100}" required title="2 to 100 characters" id="people" type="people" class="form-control" name="people" value="{{ old('people') }}" required autofocus>

                                  @if ($errors->has('people'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('people') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="col-md-8">
                                  <button 
                                    type="submit" 
                                    class="btn btn-primary btn-sm"
                                    dusk="collab-people-search-button"
                                  >
                                      Find
                                  </button>
                              </div>
                          </div>
                      </form>
                      @isset($userSearchResults)
                        @if(count($userSearchResults)>0)
                          <div class="table-responsive">
                            <table class="table table-hover table-collab-people-search">
                              <thead>
                                  <tr>
                                      <th>Name</th>
                                      <th>Email</th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>

                                @foreach($userSearchResults as $user)
                                  <tr>
                                      <td>{{$user->firstName}} {{$user->lastName}}</td>
                                      <td>{{$user->email}}</td>
                                      <td>
                                        <form class="" action="{{route('studies.collab.add',$study)}}" method="post">
                                          @csrf
                                          <input type="text" name="user_id" value="{{$user->id}}" hidden>
                                          <button 
                                            class="btn btn-primary btn-sm"
                                            type="submit"
                                            dusk="collab-{{$user->id}}-share-button"
                                          >
                                            Share
                                          </button>
                                        </form>

                                  </tr>
                                @endforeach

                              </tbody>
                            </table>
                          </div>
                        @endif
                      @endIsset
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container mb-2">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <!-- UNREGISTERED USERS -->
            <div class="card">
                <div class="card-header">
                    <b>Add an Unregistered User by Email</b>
                </div>

                <div class="card-body">
                    <div class = "container">
                        <form class="form-horizontal" method="POST" action="{{ route('assignInvitation', $study) }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        Send Invitation
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- COLLABORATORS TABLE -->
<div class="container mb-2">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <b>Collaborators of: {{$study->name}}</b>
        </div>
        <div class="card-body">
          <div class ="container">
            <div class="table-responsive">
              <table class="table table-hover table-collab-people">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                    @if($user->can('access'.$study->id)&&!$user->hasrole('admin'))
                      <tr>
                        <td>{{$user->firstName}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                          @if($user->id!==$study->user_id)
                            <form class="" action="{{route('studies.collab.remove',$study)}}" method="post">
                              @csrf
                              <input type="text" name="user_id" value="{{$user->id}}" hidden>
                              <button 
                                class="btn btn-primary btn-sm"
                                type="submit"
                                dusk="collab-{{$user->id}}-unshare-button"
                              >
                                Unshare
                              </button>
                            </form>
                          @endif
                          @if($user->id===$study->user_id)
                            Owner
                          @endif
                        </td>
                      </tr>
                    @endif
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
