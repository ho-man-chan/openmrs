@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <b>Basis Set Options</b>
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route('studies.store') }}" enctype="multipart/form-data">
            @csrf

            @foreach($study->basis as $basis)
            <label>{{$basis->seq}} Basis File Upload</label>
            <div class="form-group row">
                <div class="col">
                    
                    <input type="file" id="basis" name="basis" class="form-control-file">
                </div>
            </div>
            @endforeach
  

    <button type="submit" name="button" value="create" class="btn btn-primary">Create Study</button>

    </form>

</div>
</div>
@endsection