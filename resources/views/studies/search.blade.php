@extends('layouts.app')

@section('content')
<div class="card">
  <div class="card-header">
    @if (!empty(request('searchStudyByName')) && !empty($studySearchResults))
    <b>Studies: <span class="badge badge-secondary">{{ $studySearchResults->count() }}</span> Results Found for "{{ request('searchStudyByName') }}"</b>
    @else
        <b>Studies: <span class="badge badge-secondary">0</span> Results Found for ""</b>
    @endif
  </div>
  <div class="card-body">
      <table class="table table-hover table-study-search-result">
          <thead>
              <tr>
                  <th>Name</th>
                  <th></th>
              </tr>
          </thead>
          <tbody>
          @if (!empty(request('searchStudyByName')) && !empty($studySearchResults))
            @foreach($studySearchResults as $study)
                    @if(auth()->user()->can('access'.$study->id))
                        <tr>
                            <td>{{ $study->name}}</td>
                            <td>
                              <a href="{{ route('studies.show', $study->id)}}" class="btn btn-light btn-sm">
                                <i class="fas fa-eye"></i> View
                              </a>
                          </td>
                        </tr>
                    @endif
                @endforeach
          @endif
          </tbody>
      </table>
  </div>


</div>
@endsection
