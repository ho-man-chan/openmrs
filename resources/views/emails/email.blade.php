@component('mail::message')
# Introduction

You have been invited to join Open-MRS !

@component('mail::button', ['url' => secure_url('/')."/register?invitation_token=$content", 'color' => 'success'])
Join
@endcomponent

Thank you,<br>
{{ config('app.name') }}
@endcomponent
