@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="justify-content-center">
        <div class="row">
            <div class="col-md-8 col-xs-12 col-sm-6 col-lg-8">
                <div class="container" style="border-bottom:1px solid black">
                    <h2>{{$user->firstName}} {{$user->lastName}}</h2>
                </div>
                <hr>
                <ul class="container details">
                    <li>
                        <p>{{$user->title}}</p>
                    </li>
                    <li>
                        <p>{{$user->facility}}</p>
                    </li>
                    <li>
                        <p>{{$user->phone}}</p>
                    </li>
                    <li>
                        <p>{{$user->email}}</p>
                    </li>
                </ul>
            </div>
        </div>
        <a href="{{ route('systems.users.edit', $user) }}" class="btn btn-primary btn-sm">Edit Profile</a>
    </div>
</div>
@endsection
