@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">User Profile | {{$user->firstName}}</div>

                <form method="POST" action="{{route('systems.users.update', $user)}}">
                    @csrf
                    @method('PATCH')

                <div class="card-body">
                  
                    {{-- <a href="{{route('systems.users.create')}}" class="btn btn-primary">Add User</a> --}}
                    {{-- <a href="" class="btn btn-primary">Add User</a> --}}
                        <div class="form-group row">
                            <div class="col">
                                <label for="firstName">First Name</label>
                                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="E.g. John" value={{$user->firstName}}>
                            </div>
                            <div class="col">
                                <label for="lastName">Last Name</label>
                                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="E.g. Doe" value={{$user->lastName}}>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <label for="email">Email Address</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="E.g. JohnDoe@gmail.com" value={{$user->email}}>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" placeholder="E.g. Technician, Researcher, etc" value={{$user->title}}>
                            </div>
                            <div class="col">
                                <label for="facility">Facility</label>
                                <input type="text" class="form-control" id="facility" name="facility" placeholder="E.g. Concordia University, Perform Center, etc" value={{$user->facility}}>
                            </div>
                        </div>
                        <div class="form-group row w-50">
                            <div class="col">
                                <label for="phone">Phone Number</label>
                                <input type="tel" class="form-control" id="phone" name="phone" placeholder="Contact phone number" value={{$user->phone}}>
                            </div>
                        </div>
                </div>
                
                <div class="card-footer text-right">
                  <button type="submit" name="editButton" class="btn btn-primary btn-sm">Edit User</button>
                  {{-- <form method="POST" action="{{route('systems.users.delete', $user)}}">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                      <button type="submit" name="deleteButton" class="btn btn-danger">Delete User</button>
                  </form> --}}
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
