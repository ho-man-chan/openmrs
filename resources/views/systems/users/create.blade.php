@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
          <form method="POST" action="{{route('systems.users.store')}}">
              @csrf
              
              <div class="card">
                  <div class="card-header">Creating a new user</div>

                  <div class="card-body">
                    
                          <div class="form-group row">
                              <div class="col">
                                  <label for="firstName">First Name</label>
                                  <input type="text" class="form-control" id="firstName" name="firstName" placeholder="E.g. John">
                              </div>
                              <div class="col">
                                  <label for="lastName">Last Name</label>
                                  <input type="text" class="form-control" id="lastName" name="lastName" placeholder="E.g. Doe">
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col">
                                  <label for="email">Email Address</label>
                                  <input type="text" class="form-control" id="email" name="email" placeholder="E.g. JohnDoe@gmail.com">
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col">
                                  <label for="title">Title</label>
                                  <input type="text" class="form-control" id="title" name="title" placeholder="E.g. Technician, Researcher, etc">
                              </div>
                              <div class="col">
                                  <label for="facility">Facility</label>
                                  <input type="text" class="form-control" id="facility" name="facility" placeholder="E.g. Concordia University, Perform Center, etc">
                              </div>
                          </div>
                          <div class="form-group row">
                              <div class="col">
                                  <label for="phone">Phone Number</label>
                                  <input type="tel" class="form-control" id="phone" name="phone" placeholder="Contact phone number">
                              </div>
                              @hasrole('admin')
                              <div class="col">
                                  <input type="radio" id="admin" name="admin" value="admin" unchecked>
                                  <label for="admin">Administrator</label>
                              </div>
                              @endhasrole
                          </div>
                  </div>
                  <div class="card-footer text-right">
                      <button type="submit" name="button" class="btn btn-primary">Create User</button>
                  </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
