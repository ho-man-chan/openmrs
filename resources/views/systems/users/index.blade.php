@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Users</div>

                <div class="card-body">

                    <a href="{{route('systems.users.create')}}" class="btn btn-primary btn-sm mb-2">Add User</a>

                    {{-- <a href="" class="btn btn-primary">Add User</a> --}}
                    <div class="table-responsive">
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td></td>
                            {{-- <td>Profile</td> --}}
                            {{-- <td>Nerd Level</td>
                            <td>Actions</td> --}}
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($users as $user)
                            <tr>
                              <td>{{$user->id}}</td>
                              <td>{{$user->firstName}}</td>
                              <td>{{$user->email}}</td>
                              <td>
                                {{-- <a href="{{ route('systems.users.profile', $user) }}" class="btn btn-primary btn-sm">View</a> --}}
                                <a href="{{ route('systems.users.edit', $user) }}" class="btn btn-light btn-sm">
                                  <i class="fas fa-eye"></i> View
                                </a>
                              </td>
                            </tr>
                          @endforeach
                          {{-- @foreach($users as $key => $value)
                          <tr>
                          <td>{{ $value->id }}</td>
                          <td>{{ $value->name }}</td>
                          <td>{{ $value->email }}</td>
                          <td>{{ $value->nerd_level }}</td>
                          
                          <!-- we will also add show, edit, and delete buttons -->
                          <td>
                          
                          <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                          <!-- we will add this later since its a little more complicated than the other two buttons -->
                          
                          <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                          <a class="btn btn-small btn-success" href="{{ URL::to('nerds/' . $value->id) }}">Show this Nerd</a>
                          
                          <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                          <a class="btn btn-small btn-info" href="{{ URL::to('nerds/' . $value->id . '/edit') }}">Edit this Nerd</a>
                          
                        </td>
                      </tr>
                    @endforeach --}}
                  </tbody>
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
