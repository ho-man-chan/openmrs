@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Your personal Info</div>

                <div class="card-body user-profile">
                  
                    {{-- <a href="{{route('systems.users.create')}}" class="btn btn-primary">Add User</a> --}}
                    {{-- <a href="" class="btn btn-primary">Add User</a> --}}

                    <form method="POST" action="{{route('systems.users.update', $user)}}">
                        {{ csrf_field() }}
                        {{ method_field('patch') }}
                        <div class="form-group row">
                            <div class="col">
                                <label for="firstName">First Name</label>
                                <input disabled type="text" class="form-control" id="firstName" name="firstName" placeholder="" value={{$user->firstName}}>
                            </div>
                            <div class="col">
                                <label for="lastName">Last Name</label>
                                <input disabled type="text" class="form-control" id="lastName" name="lastName" placeholder="" value={{$user->lastName}}>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <label for="email">Email Address</label>
                                <input disabled type="text" class="form-control" id="email" name="email" placeholder="" value={{$user->email}}>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <label for="title">Title</label>
                                <input disabled type="text" class="form-control" id="title" name="title" placeholder="" value={{$user->title}}>
                            </div>
                            <div class="col">
                                <label for="facility">Facility</label>
                                <input disabled type="text" class="form-control" id="facility" name="facility" placeholder="" value={{$user->facility}}>
                            </div>
                        </div>
                        <div class="form-group row w-50">
                            <div class="col">
                                <label for="phone">Phone Number</label>
                                <input disabled type="tel" class="form-control" id="phone" name="phone" placeholder="" value={{$user->phone}}>
                            </div>
                        </div>


                        {{-- <button type="submit" name="editButton" class="btn btn-primary">Edit User</button> --}}

                    </form>
                    {{-- <form method="POST" action="{{route('systems.users.delete', $user)}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" name="deleteButton" class="btn btn-danger">Delete User</button>
                    </form> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
