@extends('layouts.session.model')

 @section('model-content')
   <table class="table table-striped table-bordered">
     <thead>
         <tr>
             <td>Type</td>
             <td>Status</td>
             <td>Download</td>
         </tr>
     </thead>
     <tbody>
     @foreach ($session->files as $file)
       <tr>
         <td>{{$file->type}}</td>
         <td>{{$file->status}}</td>
         <td><a href="" class="btn">Download</a></td>
       </tr>
     @endforeach
     </tbody>
   </table>
@endsection 