@extends('layouts.session.model')
 @section('model-content')

   @foreach($analyzedFiles as $analyzedFile)
     <embed
       src="{{route('analysis.display',['id'=>"$analyzedFile->path"])}}"
       type="application/pdf"
       width="100%"
       height="600px"
     />
   @endforeach
   <!-- <button type="submit" name="deleteButton" class="btn btn-danger">Delete Analysis</button> -->
@endsection
