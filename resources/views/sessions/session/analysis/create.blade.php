@extends('layouts.session.model')

@section('model-content')
<div class="card">
    <div class="card-header">TARQUIN Data Analysis</div>

  <form method="POST" action="{{route('sessions.analysis.store', [$session])}}">
    @csrf
    <div class="card-body">
            <div class="form-group row">
                <div class="col">
                    <label for="analysisName">Name of Analysis</label>
                    <input type="text" class="form-control" id="analysisName" name="analysisName"
                    
                    {{-- placeholder="Name of analysis" value="Analysis # {{optional($count + 1)}}" --}}
                    
                    >
                </div>
            </div>

            <div class="form-group row">
                <div class="col">
                    <label for="samplingFreq">Sampling Frequency (Hz)</label>
                    <input type="text" class="form-control" id="samplingFreq" name="samplingFreq"
                    
                    {{-- value={{optional($analysis->sampling_freq)}} --}}
                    
                    >
                </div>
                <div class="col">
                    <label for="transmitterFreq">Transmitter Frequency (Hz)</label>
                    <input type="text" class="form-control" id="transmitterFreq" name="transmitterFreq" 
                    
                    {{-- value={{optional($analysis->transmitter_freq)}} --}}
                    >
                </div>
            </div>

            <div class="form-group row">
                <div class="col">
                    <label for="echoTime">Echo Time</label>
                    <input type="text" class="form-control" id="echoTime" name="echoTime" 
                    
                    {{-- value={{optional($analysis->echo_time)}} --}}
                    
                    >
                </div>
                <div class="col">
                    <label for="startPoint">Start Point</label>
                    <input type="text" class="form-control" id="startPoint" name="startPoint" 
                    
                    {{-- value={{optional($analysis->start_point)}} --}}
                    
                    >
                </div>
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect1">Scan Type</label>
              <select class="form-control" id="scanType" name="scanType">
                <option>Press</option>
                <option>Mega-Press</option>
                <option>All Scans</option>
              </select>
            </div>

            <div class="form-check row">
                <div class="col">
                    <input type="checkbox" class="form-check-input" id="eddyCurrentCorrection" name="eddyCurrentCorrection">
                    <label class="form-check-label" for="eddyCurrentCorrection">Eddy current correction</label>
                </div>
                <div class="col">
                    <input type="checkbox" class="form-check-input" id="stackPDF" name="stackPDF" 
                    
                    {{-- checked={{optional($analysis->stack_pdf)}} --}}
                    >
                    
                    <label class="form-check-label" for="stackPDF">Stack PDF</label>
                </div>
            </div>

            

    </div>
    <div class="card-footer text-right">
      <button type="submit" name="button" class="btn btn-primary btn-sm">Create New Analysis</button>
    </div>
    </form>
</div>
@endsection
