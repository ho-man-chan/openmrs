@extends('layouts.session.model')

@section('model-content')
  <script id="volume-ui-template" type="x-volume-ui-template">
    <div class="volume-viewer-display">
    </div>
    <div class="volume-viewer-controls volume-controls">

      <div class="coords">
        <div class="control-heading" id="voxel-coordinates-heading-@{{VOLID}}">
          Voxel Coordinates:
        </div>
        <div class="voxel-coords" data-volume-id="@{{VOLID}}">
          I:<input id="voxel-i-@{{VOLID}}" class="control-inputs">
          J:<input id="voxel-j-@{{VOLID}}" class="control-inputs">
          K:<input id="voxel-k-@{{VOLID}}" class="control-inputs">
        </div>
        <div class="control-heading" id="world-coordinates-heading-@{{VOLID}}">
          World Coordinates:
        </div>
        <div class="world-coords" data-volume-id="@{{VOLID}}">
          X:<input id="world-x-@{{VOLID}}" class="control-inputs">
          Y:<input id="world-y-@{{VOLID}}" class="control-inputs">
          Z:<input id="world-z-@{{VOLID}}" class="control-inputs">
        </div>
      </div>

      <div id="intensity-value-div-@{{VOLID}}">
        <span class="control-heading" data-volume-id="@{{VOLID}}">
          Value:
        </span>
        <span id="intensity-value-@{{VOLID}}" class="intensity-value"></span>
      </div>

      <div class="threshold-div" data-volume-id="@{{VOLID}}">
        <div class="control-heading">
          Threshold:
        </div>
        <div class="thresh-inputs">
          <input id="min-threshold-@{{VOLID}}" class="control-inputs thresh-input-left" value="0"/>
          <input id="max-threshold-@{{VOLID}}" class="control-inputs thresh-input-right" value="255"/>
        </div>
        <div class="slider volume-viewer-threshold" id="threshold-slider-@{{VOLID}}"></div>
      </div>

      <div class="contrast-div" data-volume-id="@{{VOLID}}">
        <span class="control-heading" id="contrast-heading@{{VOLID}}">Contrast (0.0 to 2.0):</span>
        <input class="control-inputs" value="1.0" id="contrast-val"/>
        <div id="contrast-slider" class="slider volume-viewer-contrast"></div>
      </div>

      <div class="brightness-div" data-volume-id="@{{VOLID}}">
        <span class="control-heading" id="brightness-heading@{{VOLID}}">Brightness (-1 to 1):</span>
        <input class="control-inputs" value="0" id="brightness-val"/>
        <div id="brightness-slider" class="slider volume-viewer-brightness"></div>
      </div>

      <div id="slice-series-@{{VOLID}}" class="slice-series-div" data-volume-id="@{{VOLID}}">
        <div class="control-heading" id="slice-series-heading-@{{VOLID}}">All slices: </div>
        <span class="slice-series-button button" data-axis="xspace">Sagittal</span>
        <span class="slice-series-button button" data-axis="yspace">Coronal</span>
        <span class="slice-series-button button" data-axis="zspace">Transverse</span>
      </div>

    </div>
  </script>
  <div id="loading" style="display: none"><img src="/vendor/brainbrowser/img/ajax-loader.gif" /></div>
  <div id="brainbrowser-wrapper" style="width: 100%; height: 100%;">
    <div id="volume-viewer" style="background-color: #4e73df; width: 100%; height: auto; border-radius: 0px;">
      <div id="global-controls" class="volume-viewer-controls" style="background-color: #4e73df;">
        <span class="control-heading" style="display: block; font-size: 20px; width: 100%; text-align: center; padding: 0.3em;">
          Nifti Volumes Size Options
          <select id="panel-size">
            <option value="300" selected>300</option>
            <option value="500">500</option>
          </select>
        </span>
        <hr class="sidebar-divider my-0">
        <div style="display: flex; padding: 0.3em;">
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            @foreach($brainBrowserFiles as $key=>$brainBrowserFile)
              <a class="nav-link" id="v-pills-{{$key}}-tab" data-toggle="pill" href="#v-pills-{{$key}}" style="border-radius: 0; max-width: 300px; text-overflow: ellipsis; color: rgba(255, 255, 255, 0.8);" onmouseover="onHover(this)" onmouseout="onHoverOut(this)" aria-selected="false" value={{route('file.download', ['id'=>$brainBrowserFile->path])}}>{{$brainBrowserFile->name}}</a>
            @endforeach
          </div>
          <div class="tab-content" id="v-pills-tabContent" style="width: 100%; display: flex; overflow: auto;">
                <div id="brainbrowser"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('css')
  <link type="text/css" href="{{asset('vendor/brainbrowser/css/ui-darkness/jquery-ui-1.8.10.custom.css')}}" rel="Stylesheet" />
  <link type="text/css" href="{{asset('vendor/brainbrowser/css/common.css')}}" rel="Stylesheet" />
  <link type="text/css" href="{{asset('vendor/brainbrowser/css/volume-viewer-demo.css')}}" rel="Stylesheet" />
@endSection

@section('js')
  <script>
    function onHover(e) {
      e.style.color = "rgba(255, 255, 255, 1.0)";
    }
    function onHoverOut(e) {
      e.style.color = "rgba(255, 255, 255, 0.8)";
    }
  </script>
  <script src="{{asset('vendor/brainbrowser/js/jquery-1.6.4.min.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/jquery-ui-1.8.10.custom.min.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/ui.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/pako.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/brainbrowser.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/core/tree-store.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/lib/config.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/lib/utils.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/lib/events.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/lib/loader.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/lib/color-map.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/lib/display.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/lib/panel.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/lib/utils.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/modules/loading.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/modules/rendering.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/volume-loaders/overlay.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/volume-loaders/minc.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/volume-loaders/nifti1.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/volume-loaders/mgh.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/volume-loaders/hdf5.js')}}"></script>
  <script src="{{asset('vendor/brainbrowser/js/brainbrowser/volume-viewer/volume-loaders/netcdf.js')}}"></script>
  <script src="{{asset('js/brainbrowser/brainbrowser-volume-custom.js')}}"></script>
  <script src="{{asset('js/brainbrowser/custom/volume-viewer-demo.config.js')}}"></script>
@endSection