@extends('layouts.session.model')

@section('model-content')
  @foreach($preprocessedFiles as $preprocessedFile)
    <embed
      src="{{route('file.display',['id'=>"$preprocessedFile->path"])}}"
      type="application/pdf"
      width="100%"
      height="600px"
    />
  @endforeach

@endsection
