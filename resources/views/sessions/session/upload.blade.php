@extends('layouts.session.model')

 @section('model-content')
   <div class="table-responsive-lg">
     <table class="table table-striped table-bordered">
       <thead>
           <tr>
               <td>Type</td>
               <td>Status</td>
               <td>Download</td>
           </tr>
       </thead>
       <tbody>
       @foreach ($session->files as $file)
         <tr>
           <td>{{$file->type}}</td>
           <td>{{$file->status}}</td>
           <td><a href="{{route('file.download',['id'=>$file->path])}}" class="btn">Download</a></td>
         </tr>
       @endforeach
       </tbody>
     </table>
   </div>

   <upload-page session="{{$session}}" fields="{{json_encode(["type","status","download"])}}"></upload-page>
@endsection 