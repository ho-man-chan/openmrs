@extends('layouts.session.model')

@section('model-content')
  @foreach($processedDicomFiles as $processedDicomFile)
    <embed
      src="{{route('file.display',['id'=>"$processedDicomFile->path"])}}"
      type="application/pdf"
      width="100%"
      height="600px"
    />
  @endforeach

@endsection
