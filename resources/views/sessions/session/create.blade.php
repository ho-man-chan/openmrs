@extends('layouts.app')

 @section('content')
  <div class="card">
      <div class="card-header">
        <b>Create Session: Upload Scans<b>
      </div>

      <form class="" action="{{route('sessions.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
          <label>Megapress</label>
          <input type="file" name="megapress" class="form-control-file">
          <label>Press</label>
          <input type="file" name="press" class="form-control-file">
          <label>Dicom Path</label>
          <input type="file" name="dicoms" class="form-control-file" multiple>
          
          <input type="hidden" name="study" value="{{$study}}">
          
        </div>
        

        <div class="card-footer text-right">
          <button 
            type="submit"
            name="button"
            class="btn btn-primary"
            dusk="session-create-button"
          >
            Create Session
          </button>
        </div>
      </form>
  </div>
@endsection
