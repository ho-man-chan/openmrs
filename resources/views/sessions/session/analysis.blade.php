@extends('layouts.session.model')

 @section('model-content')
  <a href="{{route('sessions.analysis.create', [$session])}}" class="btn btn-primary btn-sm mb-2">Add Analysis</a>
   <div class="table-responsive">
     <table class="table table-hover">
       <thead>
         <tr>
           <td>ID</td>
           <td>Name</td>
           <td>Type</td>
           <td>Date</td>
           <td></td>
         </tr>
       </thead>
       <tbody>
         @foreach ($session->analyses as $analysis)
           <tr>
             <td>{{$analysis->id}}</td>
             <td>{{$analysis->name}}</td>
             <td>{{$analysis->type}}</td>
             <td>{{$analysis->created_at}}</td>
             <td>
               <a href="{{ route('sessions.analysis.show', ["session" => $session, "analysis"=>$analysis]) }}" class="btn btn-light btn-sm">
                 <i class="fas fa-eye"></i> View
               </a>
             </td>
           </tr>
         @endforeach
       </tbody>
     </table>
   </div>
@endsection
