@extends('layouts.app')

 @section('content')
  <div class="card">
      <div class="card-header">
        <b>Sessions: {{$session->id}}</b>
      </div>
      <div class="card-body">
        <div class="container" style="max-width: 100%;">
          <div class="row">
            <div class="col-3">
              <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link" href="#">Uploads</a>
                </li>
              </ul>
            </div>
            <div class="col-9">
              <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>Type</td>
                        <td>Status</td>
                        <td>Download</td>
                    </tr>
                </thead>
                <tbody>
                @foreach ($session->files as $file)
                  <tr>
                    <td>{{$file->type}}</td>
                    <td>{{$file->status}}</td>
                    <td><a href="" class="btn disabled">Download</a></td>
                    {{-- <td><a href="{{Storage::url($file->path)}}">Download</a></td> --}}
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
@endsection
