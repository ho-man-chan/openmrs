@extends('layouts.app')

@section('content')
  <div class="card">
    <div class="card-header">
        Sessions in {{$study->name}}
        <div class="float-right">
          <a 
            href="{{route('studies.collab',['study' => $study->id])}}"
            class="btn btn-success btn-sm"
            dusk="collab-button"
          >
            <i class="fas fa-user-friends"></i> Share
          </a>          
        </div>
    </div>
    <div class="card-body">
      <div class="col-12 mb-2">
        <a
          href="{{ route('sessions.create',['study'=>$study])  }}"
          class="btn btn-primary btn-sm"
          dusk="session-add-button"
        >
          Add Session
        </a>
      </div>
      <div class="col-12">
        <table class="table table-hover">
          <thead>
              <tr>
                  <th>ID</th>
                  <th>Date</th>
                  <th></th>
              </tr>
          </thead>
          <tbody>
          @foreach ($sessions as $session)
            <tr>
                <td>{{$session->id}}</td>
                <td>{{$session->created_at}}</td>
                <td>
                    <a href="{{route('sessions.show', ['study' => $study, 'session' => $session])}}"
                      class="btn btn-light btn-sm">
                      <i class="fas fa-eye"></i> View
                    </a>
                </td>
                {{--<td><a href="{{route('sessions.destory',['id' => 1])}}" class="btn btn-danger btn-sm">Delete</a></td> --}}
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection 

