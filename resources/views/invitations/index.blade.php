@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
            
                @if (session('error'))
                    <div class="alert alert-danger">
                        <p>{{ session('error') }}</p>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        <p>{{ session('success') }}</p>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <b>Invitation Requests Pending: <span class="badge badge-secondary">{{ count($invitations) }}</span></b>
                    </div>
                        <div class="card-body">
                            <div class="container">
                                @if (!empty($invitations))
                                    <table class="table table-hover table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Email</th>
                                                <th>Created</th>
                                                <th>Invitation Link</th>
                                                <td></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($invitations as $invitation)
                                                <tr>
                                                    <td><a href="mailto:{{ $invitation->email }}">{{ $invitation->email }}</a></td>
                                                    <td>{{ $invitation->created_at }}</td>
                                                    <td>
                                                        <kbd>{{ $invitation->getLink() }}</kbd>
                                                    </td>
                                                    <td>
                                                         <a href="{{route('sendInvitation', ['email'=>$invitation->email, 'token'=>$invitation->invitation_token])}}" class="btn btn-primary btn-sm">Invite</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p>No invitation requests!</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection                            