<!-- Session sidebar nav -->
<nav class="nav flex-column">
  {{-- <a href="{{route('sessions.create')}}" class="btn btn-primary btn">Add Session</a>
  <a class="nav-link {{ request()->is('sessions/*/') ? 'active' : '' }}" href="{{route('sessions.index')}}">Sessions</a>
  <a class="nav-link {{ request()->is('sessions/*/') ? 'active' : '' }}" href="{{route('sessions.detail')}}">Details</a>
  <a class="nav-link" href="{{route('sessions.analysis')}}">Analysis</a>
  <a class="nav-link" href="{{route('sessions.proprocess')}}">Preprocess</a>
  <a class="nav-link" href="{{route('sessions.upload')}}">Uploads</a>
  <a class="nav-link" href="{{route('sessions.delete')}}">Delete</a> --}}
  
  @php ($tabs = ['analysis'=>'Analysis','preprocess'=>'Preprocess', 'voxel'=>'Voxel','brainbrowser'=>'BrainBrowser', 'upload'=>'Uploads',])
  @foreach ($tabs as $tab => $name)
    <a
      class="nav-link  
            {{ request()->is("sessions/*/$tab") ? 'active' : '' }}
            " 
      {{-- href="{{route("sessions.$tab",['study'=>$study,'session'=>$session])}}">{{$name}} --}}
      href="{{route("sessions.$tab",['session'=>$session])}}">{{$name}}
    </a>
  @endforeach
</nav>  