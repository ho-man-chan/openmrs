@extends('layouts.app')

 @section('content')
   <div class="card">
      <div class="card-header">Sessions: {{$session->id}}</div>

      <div class="card-body">
        
        <div class="container" style="max-width:100%;">
          <div class="row">
            <div class="col-12">
              <div class="btn-toolbar mb-2" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group mr-2" role="group" aria-label="First group">
                  @php ($tabs = ['analysis'=>'Analysis','preprocess'=>'Preprocess', 'voxel'=>'Voxel', 'brainbrowser'=>'BrainBrowser', 'upload'=>'Uploads',])
                  @foreach ($tabs as $tab => $name)
                    <a
                      class="btn btn-secondary btn-sm
                            {{ request()->is("sessions/*/$tab") ? 'active' : '' }}
                            " 
                      {{-- href="{{route("sessions.$tab",['study'=>$study,'session'=>$session])}}">{{$name}} --}}
                      href="{{route("sessions.$tab",['session'=>$session])}}">{{$name}}
                    </a>
                  @endforeach
                </div>
              </div>
              @yield('model-content')
            </div>
          </div>
        </div>

      </div>
  </div>
@endsection
