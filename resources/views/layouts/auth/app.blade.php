@include('includes.header')

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        @yield('content');

      </div>

    </div>

  </div>
@include('includes.footer')
