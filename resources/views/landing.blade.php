@extends('layouts.landing')

@section('content')
  <!-- Masthead -->
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Open MRS</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5">A platform to preprocess, analyze and organize </br> 
          magnetic resonanse spectroscopy data</p>
          <a class="btn btn-primary btn-xl" href="{{route('register')}}">Get Started</a>
        </div>
      </div>
    </div>
  </header>
<!-- Services Section -->
<section class="page-section" id="services">
    <div class="container">
      <h2 class="text-center mt-0">At Your Service</h2>
      <hr class="divider my-4">
      <div class="row">
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-sitemap text-primary mb-4"></i>
            <h3 class="h4 mb-2">Organize Data</h3>
            <p class="text-muted mb-0">Structure your data and share your findings with other users</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-laptop-code text-primary mb-4"></i>
            <h3 class="h4 mb-2">Preprocess Scans</h3>
            <p class="text-muted mb-0">Easily preprocess Mega-Press or Press scans using FID-A</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-chart-line text-primary mb-4"></i>
            <h3 class="h4 mb-2">Analyze</h3>
            <p class="text-muted mb-0">Fit and analyze data to find metabolite concentrations with TARQUIN</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-brain text-primary mb-4"></i>
            <h3 class="h4 mb-2">Visualize</h3>
            <p class="text-muted mb-0">view your scan locations with the embedded DICOM viewer </p>
          </div>
        </div>
      </div>
    </div>
  </section>

    <!-- Contact Section -->
    <!-- <section class="page-section" id="contact">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="mt-0">Let's Get In Touch!</h2>
          <hr class="divider my-4">
          <p class="text-muted mb-5">Ready to start your next project with us? Give us a call or send us an email and we will get back to you as soon as possible!</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 ml-auto text-center">
          <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
          <div>+1 (202) 555-0149</div>
        </div>
        <div class="col-lg-4 mr-auto text-center">
          <i class="fas fa-envelope fa-3x mb-3 text-muted"></i> -->
          <!-- Make sure to change the email address in anchor text AND the link below! -->
          <!-- <a class="d-block" href="mailto:contact@yourwebsite.com">contact@yourwebsite.com</a>
        </div>
      </div>
    </div>
  </section> -->
  @endsection