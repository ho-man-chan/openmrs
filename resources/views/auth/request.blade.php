@extends('layouts.auth.app')

@section('content')
  <div class="card o-hidden border-0 shadow-lg my-5">
    
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> --}}
        <div class="col-lg-6 offset-lg-3">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">Requesting Invitation</h1>
              <p>{{ config('app.name') }} is a closed community. You must have an invitation link to register. You can request your link below.</p>
              @if (session('error'))
                  <div class="alert alert-danger">
                      <p>{{ session('error') }}</p>
                  </div>
              @endif

              @if (session('success'))
                  <div class="alert alert-success">
                      <p>{{ session('success') }}</p>
                  </div>
              @endif
            </div>
            <form class="user" method="POST" action="{{ route('storeInvitation') }}">
              @csrf
            
              {{-- @if ($errors->has('email'))
                <div class="my-2 text-center" style="color: #e74a3b">
                  <small>This email has already used.</small>
                </div>    
              @endif --}}
            
              <div class="form-group">
                <input type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="{{ __('Enter E-Mail Address') }}" name="email" value="{{ old('email') }}" required autofocus>
              </div>
              
              <button type="submit" class="btn btn-primary btn-user btn-block">
                Request An Invitation
              </button>
            </form>
            <hr>
            @if (Route::has('password.request'))
              <div class="text-center">
                <a class="small" href="{{ route('password.request') }}">{{ __('Forgot Password?') }}</a>
              </div>
            @endif
              <div class="text-center">
                <a class="small" href="{{ route('login') }}">Already have an account? Login!</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
