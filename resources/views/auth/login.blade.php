@extends('layouts.auth.app')

@section('content')
  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> --}}
        <div class="col-lg-6 offset-lg-3">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">{{ __('Login') }}</h1>
            </div>
            <form class="user"method="POST" action="{{ route('login') }}">
              @csrf

              @if ($errors->has('email')||$errors->has('username')||$errors->has('password'))
                <div class="my-2 text-center" style="color: #e74a3b">
                  <small>Wrong email or password. </br>Try again or click Forgot password ro reset it.</small>
                </div>
              @endif

              <div class="form-group">
                <input type="text" class="form-control form-control-user" id="exampleInputLogin" aria-describedby="emailHelp" placeholder="{{ __('Enter E-Mail or Username') }}" name="login" value="{{ old('email') ?: old('username')}}" required autofocus>
              </div>

              <div class="form-group">
                <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="{{ __('Password') }}" name="password" required>
              </div>

              <div class="form-group">
                <div class="custom-control custom-checkbox small">
                  <input type="checkbox" class="custom-control-input" id="customCheck" name="remember" {{ old('remember') ? 'checked' : '' }}>
                  <label class="custom-control-label" for="customCheck">{{ __('Remember Me') }}</label>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-user btn-block">
                {{ __('Login') }}
              </button>
            </form>
            <hr>
            @if (Route::has('password.request'))
              <div class="text-center">
                <a class="small" href="{{ route('password.request') }}">{{ __('Forgot Password?') }}</a>
              </div>
            @endif
              <div class="text-center">
                <a class="small" href="{{ route('requestInvitation') }}">{{ __('Create a Account!') }}</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
