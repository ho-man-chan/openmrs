@extends('layouts.auth.app')

@section('content')
  
  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">{{ __('Login') }}</h1>
            </div>
            <form class="user"method="POST" action="{{ route('password.update') }}">
              @csrf

              <input type="hidden" name="token" value="{{ $token }}">
              
              <div class="form-group">
                {{-- <input 
                  id="email"
                  type="email"
                  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                  name="email"
                  value="{{ $email ?? old('email') }}" 
                  placeholder="{{ __('Enter E-Mail or Username') }}"
                  required
                  autofocus
                > --}}
                
                <input type="text" class="form-control form-control-user" id="exampleInputLogin" aria-describedby="emailHelp" placeholder="{{ __('Enter E-Mail or Username') }}" name="login" value="{{ old('email') ?: old('username')}}" required autofocus>
              </div>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                
              </div>

              <button type="submit" class="btn btn-primary btn-user btn-block">
                {{ __('Reset Password') }}
              </button>
            </form>
            <hr>
            @if (Route::has('password.request'))
              <div class="text-center">
                <a class="small" href="{{ route('password.request') }}">{{ __('Forgot Password?') }}</a>
              </div>
            @endif
              <div class="text-center">
                <a class="small" href="{{ route('requestInvitation') }}">{{ __('Create a Account!') }}</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
