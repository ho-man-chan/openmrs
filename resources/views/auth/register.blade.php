@extends('layouts.auth.app')

@section('content')
  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> --}}
        <div class="col-lg-6 offset-lg-3">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">{{ __('Register') }}</h1>
            </div>
            <form class="user" method="POST" action="{{ route('register') }}">
              @csrf
              
              <input type="text" name="invitation_token" value="{{$invitation_token}}" hidden>

              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              {{-- @if ($errors->has('email')||$errors->has('username')||$errors->has('password'))
                <div class="my-2 text-center" style="color: #e74a3b">
                  <small>Wrong email or password. </br>Try again or click Forgot password ro reset it.</small>
                </div>
              @endif --}}

              <div class="form-group">
                <input type="text" class="form-control form-control-user" placeholder="{{ __('Pick a Username') }}" name="username" value="{{ old('username')}}" required autofocus>
              </div>
              
              <div class="form-group">
                <input type="text" class="form-control form-control-user" placeholder="{{ __('Enter Your First Name') }}" name="first_name" value="{{ old('first_name')}}" required autofocus>
              </div>
              
              <div class="form-group">
                <input type="text" class="form-control form-control-user" placeholder="{{ __('Enter Your Last Name') }}" name="last_name" value="{{ old('last_name')}}" required autofocus>
              </div>

              <div class="form-group">
                <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="{{ __('Password') }}" name="password" required>
              </div>
              
              <div class="form-group">
                <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="{{ __('Password Confirm') }}" name="password_confirmation" required>
              </div>

              <button type="submit" class="btn btn-primary btn-user btn-block">
                {{ __('Register') }}
              </button>
            </form>
            <hr>
            @if (Route::has('password.request'))
              <div class="text-center">
                <a class="small" href="{{ route('password.request') }}">{{ __('Forgot Password?') }}</a>
              </div>
            @endif
              <div class="text-center">
                <a class="small" href="{{ route('requestInvitation') }}">{{ __('Create a Account!') }}</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
{{-- <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <b>Register</b>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control disabled" value="{{ $email }}" disabled>
                                <input id="email" type="hidden" class="form-control disabled" name="email" value="{{ $email }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection