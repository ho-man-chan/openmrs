<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<!-- Custom scripts for all pages-->
<script type="text/javascript" src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<!-- Specific scripts for certain pages -->
@yield('js')