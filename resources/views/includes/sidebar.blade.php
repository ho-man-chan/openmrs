<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/home') }}">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-brain"></i>
    </div>
    <div class="sidebar-brand-text mx-3">OpenMRS</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Divider -->
  <hr class="sidebar-divider">

  <li class="nav-item">
    <a class="nav-link" href="{{route('studies.index')}}">
      <i class="fas fa-fw fa-folder"></i>
      <span>Studies</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">
      <i class="fas fa-fw fa-history"></i>
      <span>Recent</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">
      <i class="fas fa-fw fa-trash"></i>
      <span>Trash</span>
    </a>
  </li>
  @if(auth()->user()->hasrole('admin'))
  <li class="nav-item">
    <a class="nav-link" href="{{ route('showInvitations') }}">
      <i class="fas fa-fw  fa-user-plus"></i>
      <span>Invitation Requests</span>
    </a>
  </li>
  @endif

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->