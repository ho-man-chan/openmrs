<?php
namespace Tests;

use App\User;
use Laravel\Dusk\Browser;

class DuskBrowser extends Browser {
  
  public function signIn(User $user = null) {
  
    if(!$user) {
      $user = factory(User::class)->create();   
    }
  
    $this->user = $user;
    $this->loginAs($user);
  
    return $this;
  }
  
  public function createAStudy(String $studyName = null, String $studyType = null) {
  
    if( is_null($studyName) ) {
        $studyName = "Alzheimer Study";
    }
    if( is_null($studyType) ) {
        $studyType = "Alzheimer";
    }
  
    $this->visit('/home')
            ->click('@study-add-button')
            ->type('studyName', $studyName)
            ->type('studyType', $studyType)
            ->click('@study-create-button');
    return $this;
  }
  
}
?>