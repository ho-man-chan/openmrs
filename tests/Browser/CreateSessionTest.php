<?php

namespace Tests\Browser;

use App\User;
use App\Study;
use Tests\DuskTestCase;
use Tests\DuskBrowser as Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateSessionTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    public function test_user_can_create_a_session_with_all_uploads_and_redirects_to_the_newly_created_session()
    {
        $this->browse(function (Browser $browser) {
            $browser->signIn()
                    ->createAStudy()
                    ->with('.table', function ($table) {
                        $table->clickLink('View');
                    })
                    ->click('@session-add-button')
                    ->attach('megapress', public_path('storage/samples/session/megapress/P40448.7'))
                    ->attach('press', public_path('storage/samples/session/press/P43008.7'))
                    ->attach('dicoms', public_path('storage/samples/session/A.zip'))
                    ->click('@session-create-button')
                    ->with('.table', function ($table) {
                        $table->assertSee('megapress');
                        $table->assertSee('press');
                        $table->assertSee('dicoms');
                    });
        });
      }
      
      public function test_user_can_create_a_session_with_no_uploads_and_redirects_to_the_newly_created_session()
      {
          $this->browse(function (Browser $browser) {
              $browser->signIn()
                      ->createAStudy()
                      ->with('.table', function ($table) {
                          $table->clickLink('View');
                      })
                      ->click('@session-add-button')
                      ->click('@session-create-button')
                      ->with('.table', function ($table) {
                        $table->assertDontSee('megapress');
                        $table->assertDontSee('press');
                        $table->assertDontSee('dicoms');
                      });
          });
        }
}
