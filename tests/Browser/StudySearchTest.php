<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Tests\DuskBrowser as Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class StudySearchTest extends DuskTestCase
{
  
  use DatabaseMigrations;
  
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_user_can_search_a_study()
    {
        $this->browse(function (Browser $browser) {
            $browser->signIn()
                    ->createAStudy()
                    ->type('searchStudyByName','Study')
                    ->click('@top-bar-study-search-button')
                    ->with('.table-study-search-result', function($table){
                      $table->assertSee('Study');
                    });
        });
    }
}
