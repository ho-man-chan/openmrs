<?php

namespace Tests\Browser;

use App\User;
use App\Study;
use Tests\DuskTestCase;
use Tests\DuskBrowser as Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CollabTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    public function test_user_can_share_a_study()
    {
        //mocking two users
        $john = factory(User::class)->create();
        $peter = factory(User::class)->create();  
           
        $this->browse(function ($johnsBrowser, $petersBrowser) use ($john, $peter){
          
            //john creates a study and shares with peter
            $johnsBrowser->signIn($john)
                          ->createAStudy()
                          ->with('.table', function ($table) {
                              $table->clickLink('View');
                          })
                          ->click('@collab-button')
                          ->type('people',$peter->firstName)
                          ->click('@collab-people-search-button')
                          ->with('.table-collab-people-search', function ($table) use ($peter) {
                              $table->assertSee($peter->firstName)
                                    ->click("@collab-$peter->id-share-button");
                          });
                          
            //peter logs in and sees the study
            $petersBrowser->signIn($peter)
                          ->visit('/home')
                          ->with('.table', function ($table) {
                              $table->assertSee('Alzheimer Study');
                          });      
        });
    }
    
    public function test_user_can_unshare_a_study()
    {
        //mocking two users
        $john = factory(User::class)->create();
        $peter = factory(User::class)->create();  
           
        $this->browse(function ($johnsBrowser, $petersBrowser) use ($john, $peter){
          
            //john creates a study and shares with peter
            $johnsBrowser->signIn($john)
                          ->createAStudy()
                          ->with('.table', function ($table) {
                                $table->clickLink('View');
                            })
                          ->click('@collab-button')
                          ->type('people',$peter->firstName)
                          ->click('@collab-people-search-button')
                          ->with('.table-collab-people-search', function ($table) use ($peter) {
                              $table->assertSee($peter->firstName)
                                    ->click("@collab-$peter->id-share-button");
                          });
                    
            //peter logs in and sees the study
            $petersBrowser->signIn($peter)
                          ->visit('/home')
                          ->with('.table', function ($table) {
                              $table->assertSee('Alzheimer Study');
                          });
            
            //john unshares study from peter              
            $johnsBrowser->with('.table-collab-people', function ($table) use ($peter) {
                $table->assertSee($peter->firstName)
                      ->click("@collab-$peter->id-unshare-button");
            });    
            
            //peter logs in and dont see the study
            $petersBrowser->visit('/home')
                          ->with('.table', function ($table) {
                              $table->assertDontSee('Alzheimer Study');
                          });  
        });
    }
}
