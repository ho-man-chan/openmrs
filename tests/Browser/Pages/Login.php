<?php

namespace Tests\Browser\Pages;

use App\User;
use Laravel\Dusk\Browser;

class Login extends Page
{
  
    public $user;
    
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/login';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
    
    public function signIn(Browser $browser, User $user = null) {
      
      if(!$user) {
        $user = factory(User::class)->create();   
      }
      
      $this->user = $user;
      $browser->loginAs($user);
      
    }
}
