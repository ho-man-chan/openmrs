<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class Study extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/studies';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@element' => '#selector',
        ];
    }
    
    public function createAStudy(Browser $browser, String $studyName, String $studyType) {
      
      if( is_null($studyName) ) {
          $studyName = "Alzheimer Study";
      }
      if( is_null($studyType) ) {
          $studyType = "Alzheimer";
      }
      $browser->click('@study-add-button')
              ->type('studyName', $studyName)
              ->type('studyType', $studyType)
              ->click('@study-create-button');
    }
    
}
