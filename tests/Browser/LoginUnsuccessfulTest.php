<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginUnsuccessfulTest extends DuskTestCase
{
  use DatabaseMigrations;
  /**
   * User cannot login with wrong password
   *
   * @return void
   */
  public function test_login_with_wrong_password()
  {
      $user = factory(User::class)->create();

      $this->browse(function ($browser) use ($user) {
          $browser->visit('/login')
                  ->type('login', $user->username)
                  ->type('password', 'wrong')
                  ->press('Login')
                  ->assertPathIsNot('/home');
      });
  }
}
