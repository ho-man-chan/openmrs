<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Tests\DuskBrowser as Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateStudyTest extends DuskTestCase
{
    use DatabaseMigrations;
    
    public function test_user_can_create_a_study_and_redirects_to_studies()
    {
        $this->browse(function (Browser $browser) {
            $browser->signIn()
                    ->createAStudy('Alzheimer Study')
                    ->assertPathIs('/studies')
                    ->assertSee('Alzheimer Study');
        });
    }
    
    public function test_user_cannot_create_a_study_with_empty_name_and_type()
    {
      $this->browse(function (Browser $browser) {
          $browser->signIn()
                  ->createAStudy('','')
                  ->assertPathIs('/studies/create');
      });
    }
}
