<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Tests\DuskBrowser as Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserSettingTest extends DuskTestCase
{

    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_user_can_go_into_their_setting()
    {
        $this->browse(function (Browser $browser) {
            $browser->signIn()
                    ->visit('/home')
                    ->click('@top-bar-user-option-dropdown')
                    ->click('@user-profile')
                    ->assertPathIs('/setting')
                    ->assertInputValue('firstName', $browser->user->firstName)
                    ->assertInputValue('lastName', $browser->user->lastName)
                    ->assertInputValue('email', $browser->user->email);
        });
    }

    public function test_user_can_logout()
    {
        $this->browse(function (Browser $browser) {
            $browser->signIn()
                    ->visit('/home')
                    ->click('@top-bar-user-option-dropdown')
                    ->click('@user-logout')
                    ->assertPathIs('/');
        });
    }
}
