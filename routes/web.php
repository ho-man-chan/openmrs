<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['reset'=>false]);

//default landing page
Route::get('/', 'HomeController@index');

//authenticated landing page
Route::get('/home', 'StudyController@index')->name('studies.index ');

//login and registration
Route::get('/login', 'Auth\LoginController@show')->name('login');
Route::get('/register', 'Auth\RegisterController@show')->name('register');

//user settings
Route::get('/setting', 'SettingController@index')->name('setting');

/*
  Admin Management
*/

Route::get('/system', 'UserController@index')->name('system');

//user management
Route::get('/system/users/create', 'UserController@create')->name('systems.users.create');
Route::get('/system/users/{user}/profile', 'UserController@profile')->name('systems.users.profile');
Route::get('/system/users/{user}/edit', 'UserController@edit')->name('systems.users.edit');
Route::get('/system/users', 'UserController@index')->name('systems.users.index');

Route::post('/system/users/store', 'UserController@store')->name('systems.users.store');
Route::patch('/system/users/{user}/update', 'UserController@update')->name('systems.users.update');
Route::delete('/system/users/{user}/delete', 'UserController@destroy')->name('systems.users.delete');

/*
  Studies
*/

Route::get('/studies', 'StudyController@index')->name('studies.index');
Route::post('/studies/store', 'StudyController@store')->name('studies.store');
Route::get('/studies/create', 'StudyController@create')->name('studies.create');
Route::post('/studies/basis', 'StudyController@basis')->name('studies.basis');
Route::post('/studies/{study}/basisOptions', 'StudyController@basisOptions')->name('studies.basisOptions');
Route::post('/studies/{study}/basisFIDA', 'StudyController@basisFIDA')->name('studies.basisFIDA');
Route::post('/studies/{study}/basisUpload', 'StudyController@basisUpload')->name('studies.basisUpload');
Route::get('/studies/{study}', 'Session\SessionController@index')->name('studies.show');

//search
Route::post('/studies/search', 'StudyController@search')->name('studies.search');
Route::get('/studies/search', 'StudyController@searchIndex')->name('studies.search.index');

//collab
Route::get('/studies/{study}/sessions/collab', 'CollabController@index')->name('studies.collab');
Route::post('/studies/{study}/sessions/collab', 'CollabController@search')->name('studies.collab.search.user');
Route::post('/studies/{study}/sessions/collab/add', 'CollabController@add')->name('studies.collab.add');
Route::post('/studies/{study}/sessions/collab/remove', 'CollabController@remove')->name('studies.collab.remove');

/*
  Session
*/

Route::resource('sessions', 'Session\SessionController', [
  'except' => ['index']
]);

Route::prefix('studies/{study}/sessions')->name('sessions.')->group(function () {
    Route::get('/', 'Session\SessionController@index')->name('index');
});

Route::prefix('sessions')->name('sessions.')->group(function () {
    Route::get('{session}/detail', 'Session\SessionController@index')->name('detail');
    Route::get('{session}/preprocess', 'Session\PreprocessController@index')->name('preprocess');
    Route::get('{session}/voxel', 'Session\DicomController@index')->name('voxel');
    Route::get('{session}/analysis', 'Session\AnalysisController@index')->name('analysis');
    Route::get('{session}/analysis/{analysis}/show', 'Session\AnalysisController@show')->name('analysis.show');
    Route::get('{session}/analysis/create', 'Session\AnalysisController@create')->name('analysis.create');
    Route::post('{session}/analysis/store', 'Session\AnalysisController@store')->name('analysis.store');
    Route::get('{session}/brainbrowser', 'Session\BrainBrowserController@index')->name('brainbrowser');
    Route::get('{session}/uploads', 'Session\UploadController@index')->name('upload');
});


//File
Route::get('file/{id}', 'FileController@display')->where('id', '.*')->name('file.display');
Route::get('download/{id}', 'FileController@download')->where('id', '.*')->name('file.download');

//Analysis results
Route::get('analysis/result/{id}', 'AnalysisResultsController@display')->where('id', '.*')->name('analysis.display');
Route::get('analysis/download/{id}', 'AnalysisResultsController@download')->where('id', '.*')->name('analysis.download');

//INVITATION
/**
 * Override the default auth register route to add middleware.
 */
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('hasInvitation');
Route::get('register/request', 'Auth\RegisterController@requestInvitation')->name('requestInvitation');

/**
 * Invitations group with auth middleware.
 * Eventhough we only have one route currently, the route group is for future updates.
 */
Route::group([
    'prefix' => 'invitations'
], function() {
    Route::get('', 'InvitationsController@index')->name('showInvitations');
});

/**
 * Route for storing the invitation. Only for guest users.
 */
Route::post('invitations', 'InvitationsController@store')->middleware('guest')->name('storeInvitation');
Route::get('invitations/send', 'InvitationsController@send')->name('sendInvitation');
Route::post('/studies/{study}/sessions/invitations/assign', 'InvitationsController@assign')->name('assignInvitation');
