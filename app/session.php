<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Session extends Model 
{  
  public function files()
  {
     return $this->hasMany('App\File');
     // return $this->morphMany('App/File','filable');
  }

  public function analyses() {
    return $this->hasMany('App\Analysis');
  }

  public function study()
  {
      return $this->belongsTo('App\Study');
  }

}
