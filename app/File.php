<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
  protected $fillable = ['path', 'type'];
  
  public function session()
  {
      return $this->belongsTo('App\Session');
  }
  
  public function study()
  {
      return $this->belongsTo('App\Study');
  }
  
  // public function filable()
  // {
  //     return $this->morphTo();
  // }
  
}
