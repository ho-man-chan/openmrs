<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalysisResult extends Model
{
  protected $fillable = ['path', 'type'];
  
  public function analysis()
  {
      return $this->belongsTo('App\Analysis');
  }
}
