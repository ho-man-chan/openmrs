<?php

namespace App\Policies;

use App\User;
use App\Study;
use App\Session;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SessionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function view(User $user, Study $study)
    {
        if ($user === null ) {
          return false;
        }
        
        if ( $user->can("access$study->id") ) {
          return true;
        }
        
        return $user->id === $study->user_id;
    }

    /**
     * Determine whether the user can create sessions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, String $study_id)
    {
  
      try {
        $study = Study::findOrFail($study_id);
      }
      catch (ModelNotFoundException $e) {
        return false;
      }
      
      if ($user === null ) {
        return false;
      }
      
      if ( $user->can("access$study->id") ) {
        return true;
      }
      
      return $user->id  === $study->user_id;
    }

    /**
     * Determine whether the user can update the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function update(User $user, Session $session)
    {
        if ($user === null ) {
          return false;
        }
        
        if ( $user->can("access$session->study_id") ) {
          return true;
        }
        
        return $user->id === $session->study->user_id;
    }

    /**
     * Determine whether the user can delete the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function delete(User $user, Session $session)
    {
      if ($user === null ) {
        return false;
      }
      
      return $user->id === $session->study->user_id;
    }

    /**
     * Determine whether the user can restore the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function restore(User $user, Session $session)
    {
      return $user->id === $session->study->user_id;
    }

    /**
     * Determine whether the user can permanently delete the session.
     *
     * @param  \App\User  $user
     * @param  \App\Session  $session
     * @return mixed
     */
    public function forceDelete(User $user, Session $session)
    {
      return $user->id === $session->study->user_id;
    }
}
