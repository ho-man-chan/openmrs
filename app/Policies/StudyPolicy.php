<?php

namespace App\Policies;

use App\User;
use App\Study;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudyPolicy
{
    use HandlesAuthorization;

    public function list(User $user)
    {
      if ($user === null) {
        return false;
      }
      
      return true;
    }
    /**
     * Determine whether the user can view the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function view(User $user, Study $study)
    {
      if ($user === null) {
        return false;
      }
      
      if ($user->can("access$study->id")) {
          return true;
      }
      
      return $user->id===$study->user_id;
    }

    /**
     * Determine whether the user can create studies.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
      if ($user === null) {
        return false;
      }
      
      return true;
    }

    /**
     * Determine whether the user can update the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function update(User $user, Study $study)
    {
        //
    }

    /**
     * Determine whether the user can delete the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function delete(User $user, Study $study)
    {
        //
    }

    /**
     * Determine whether the user can restore the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function restore(User $user, Study $study)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the study.
     *
     * @param  \App\User  $user
     * @param  \App\Study  $study
     * @return mixed
     */
    public function forceDelete(User $user, Study $study)
    {
        //
    }
    
    public function collabIndex(User $user, Study $study)
    {
      
      if ($user->can("access$study->id")) {
        return true;
      }
      
      if ($user === null) {
        return false;
      }
      
      return $user->id === $study->user_id;
    }
    
    public function collabAdd(User $user, Study $study)
    {
      
      if ($user->can("access$study->id")) {
        return true;
      }
      if ($user === null) {
        return false;
      }
      
      return $user->id === $study->user_id;
    }
    
    public function collabRemove(User $user, Study $study, String $removeUserId)
    {
      
      if ($user === null) {
        return false;
      }
      
      if ($removeUserId == $study->user_id) {
          return false;
      }
      
      if ($user->can("access$study->id")) {
        return true;
      }
      
      return $user->id === $study->user_id;
    }
    
    public function collabSearch(User $user, Study $study)
    {
      
      if ($user === null) {
        return false;
      }
      
      if ($user->can("access$study->id")) {
        return true;
      }
      
      return $user->id === $study->user_id;
    }
    
    
}
