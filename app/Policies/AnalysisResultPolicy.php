<?php

namespace App\Policies;

use App\User;
use App\AnalysisResult;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnalysisResultPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the analysis result.
     *
     * @param  \App\User  $user
     * @param  \App\AnalysisResult  $analysisResult
     * @return mixed
     */
    public function view(User $user, String $analysisResultId)
    {
            
      if ($user===null) {
        return false;
      }
      
      try {
        $analysisResult = AnalysisResult::where('path', $analysisResultId)->first();
        $study_id = $analysisResult->session->study_id;
        if( $user->can("access$study_id") ) {
          return true;
        }
      
      } catch (ModelNotFoundException $e) {
        return false;
      }
      
      return $user->id === $analysisResult->session->study->user_id;
    }

    /**
     * Determine whether the user can create analysis results.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the analysis result.
     *
     * @param  \App\User  $user
     * @param  \App\AnalysisResult  $analysisResult
     * @return mixed
     */
    public function update(User $user, AnalysisResult $analysisResult)
    {
        //
    }

    /**
     * Determine whether the user can delete the analysis result.
     *
     * @param  \App\User  $user
     * @param  \App\AnalysisResult  $analysisResult
     * @return mixed
     */
    public function delete(User $user, AnalysisResult $analysisResult)
    {
        //
    }

    /**
     * Determine whether the user can restore the analysis result.
     *
     * @param  \App\User  $user
     * @param  \App\AnalysisResult  $analysisResult
     * @return mixed
     */
    public function restore(User $user, AnalysisResult $analysisResult)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the analysis result.
     *
     * @param  \App\User  $user
     * @param  \App\AnalysisResult  $analysisResult
     * @return mixed
     */
    public function forceDelete(User $user, AnalysisResult $analysisResult)
    {
        //
    }
}
