<?php

namespace App\Policies;

use App\User;
use App\Analysis;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnalysisPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the analysis.
     *
     * @param  \App\User  $user
     * @param  \App\Analysis  $analysis
     * @return mixed
     */
    public function view(User $user, Analysis $analysis)
    {  
        $study_id = $analysis->session->study_id;
        $owner_id = $analysis->session->study->user_id;
        
        if($user===null) {
            return false;
        }
        
        if($user->can("access$study_id")) {
            return true;
        } 
        
        return $user->id===$owner_id;
    }

    /**
     * Determine whether the user can create analyses.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the analysis.
     *
     * @param  \App\User  $user
     * @param  \App\Analysis  $analysis
     * @return mixed
     */
    public function update(User $user, Analysis $analysis)
    {
        //
    }

    /**
     * Determine whether the user can delete the analysis.
     *
     * @param  \App\User  $user
     * @param  \App\Analysis  $analysis
     * @return mixed
     */
    public function delete(User $user, Analysis $analysis)
    {
        //
    }

    /**
     * Determine whether the user can restore the analysis.
     *
     * @param  \App\User  $user
     * @param  \App\Analysis  $analysis
     * @return mixed
     */
    public function restore(User $user, Analysis $analysis)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the analysis.
     *
     * @param  \App\User  $user
     * @param  \App\Analysis  $analysis
     * @return mixed
     */
    public function forceDelete(User $user, Analysis $analysis)
    {
        //
    }
}
