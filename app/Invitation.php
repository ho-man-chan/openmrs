<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Invitation extends Model
{
    protected $fillable = [
        'email', 
        'invitation_token', 
        'assign_permission', 
        'registered_at',
    ];

    public function __construct()
    {
        $this->generateInvitationToken();
    } 
    
    public function generateInvitationToken() {
        // $this->invitation_token = substr(md5(rand(0, 9) . $this->email . time()), 0, 32);
        $this->invitation_token = Str::random(32);
    }

    public function getLink() {
        return urldecode(route('register') . '?invitation_token=' . $this->invitation_token);
    }
}
