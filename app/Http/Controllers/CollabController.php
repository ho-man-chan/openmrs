<?php

namespace App\Http\Controllers;

use App\User;
use App\Session;
use App\Study;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Mail\Email;
use Illuminate\Support\Facades\Mail;
use Spatie\Searchable\Search;
use Illuminate\Support\Facades\Log;
use Exception;

# Include the Autoloader (see "Libraries" for install instructions)
/* require 'vendor/autoload.php';
use Mailgun\Mailgun; */

//use Illuminate\Support\Facades\Route;

class CollabController extends Controller
{
    public function __construct(Request $request)
    {
      $this->middleware("auth");
    }

    public function index(Study $study)
    {
      
      $this->authorize('collabIndex', $study);
      
      // get all users that has access to study
      $users = User::all()->except(Auth::id());

      $users = $users->reject(function ($user, $key) use ($study) {
          return !$user->can("access$study->id");
      });

      return view('studies.collab', compact('users', 'study'));

    }

    public function add(Study $study, Request $request)
    {
      
        $this->authorize('collabAdd', $study);

        $user = null;

        try {
            $user = User::findOrFail($request->input('user_id'));
        } catch(Exception $e) {
          return abort('400');
        }

        $permission = 'access'.$study->id;
        $user->givePermissionTo($permission);

        return Redirect::back()->with('success','Operation Successful !');

    }

    public function remove(Study $study, Request $request)
    {
      
        $this->authorize('collabRemove', [$study, $request->user_id]);

        $user = null;
        
        try {
            $user = User::findOrFail($request->input('user_id'));

        } catch(ModelNotFoundException $e) {
            return abort('400');
        }

        $permission = 'access'.$study->id;
        $user->revokePermissionTo($permission);
        return Redirect::back()->with('success','Operation Successful !');

    }

    public function search(Study $study, Request $request)
    {
      
        $this->authorize('collabSearch', $study);

        $validatedData = $request->validate([
           'people' => 'nullable|min:2|max:100',
       ]);

        // get all the users except current user
        $users = User::all()->except(Auth::id());

        $users = $users->reject(function ($user, $key) use ($study) {
            return !$user->can("access$study->id");
        });

        // user search
        $usersFiltered = User::where('firstName', 'like', "%{$request->input('people')}%")
                  ->orWhere('lastName','like',"%{$request->input('people')}%")
                  ->where('id','!=',Auth::id())
                  ->take(10)
                  ->get();

        // get users without current access to Model
        $userSearchResults = $usersFiltered->reject(function ($user, $key) use ($study) {
            return $user->can("access$study->id");
        });

        return view('studies.collab', compact('study','users','userSearchResults'));

    }
}
