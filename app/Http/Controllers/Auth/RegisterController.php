<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Invitation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
          'invitation_token' => 'bail|required|exists:invitations,invitation_token',
          'username' => 'required|unique:users|max:50',
          'first_name' => 'nullable|max:50',
          'last_name' => 'nullable|max:50',
          'password' => 'required|confirmed|max:100',
          'password_confirmation' => 'required|max:100',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

      try {
        $invitation = Invitation::where('invitation_token', $data['invitation_token'])->first();
        $invitation->registered_at = Carbon::now();

        // register
        $user = User::create([
            'username' => $data['username'],
            'firstName' => $data['first_name'],
            'lastName' => $data['last_name'],
            'email' => $invitation->email,
            'password' => Hash::make($data['password']),
        ]);

        if(isset($invitation->assign_permission))
        {
            $user->givePermissionTo($invitation->assign_permission);
            $user->save();
        }

        $invitation->delete();
      } catch(ModelNotFoundException $e) {
          //throw error
      } finally {
          //throw error
      }


      return $user;
    }

    public function requestInvitation() {
        return view('auth.request');
    }

    public function showRegistrationForm(Request $request)
    {
        // $this->authorize('show', Invitation::class, $request->invitation_token);

        return view('auth.register')->with('invitation_token',$request->invitation_token);
    }

    protected function show() {
        return view('auth.register');
    }
}
