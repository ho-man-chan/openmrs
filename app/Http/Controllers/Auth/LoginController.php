<?php

namespace App\Http\Controllers\Auth;

use Adldap\AdldapInterface;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * @var Adldap
     */
    protected $ldap;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->username = $this->findUsername();
    }

    public function show() {
        return view('auth.login');
    }

    public function findUsername()
    {
      $login = request()->input('login');
      $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

      //Force authentication to be single type (email or username) but not both
      $fieldType = env('AUTH_PROVIDER') == 'ldap' ? env('AUTH_IDENTIFIER') : $fieldType;
      
      request()->merge([$fieldType => $login]);
      return $fieldType;
    }

    public function username()
    {
      return $this->username;
    }
}
