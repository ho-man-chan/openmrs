<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInvitationRequest;
use App\Invitation;
use App\Study;
use Illuminate\Http\Request;
use App\Mail\Email;
use Illuminate\Support\Facades\Mail;
use Redirect;
use DB;

class InvitationsController extends Controller
{
  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    public function index()
    {
        $this->authorize('list', Invitation::class);
        $invitations = Invitation::where('registered_at', null)->orderBy('created_at', 'desc')->get();
        return view('invitations.index', compact('invitations'));
    }

    public function store(StoreInvitationRequest $request)
    {
      
        $this->authorize('create', Invitation::class);
        $validatedData = $request->validate([
          'email' => 'required|max:100',
        ]);
        
        $invitation = new Invitation();
        $invitation->email = $request->email;
        $invitation->save();
        
        return redirect()->route('requestInvitation')
            ->with('success', 'Invitation to register successfully requested. Please wait for registration link.');
    }

    public function send(Request $request)
    {   
        $this->authorize('create', Invitation::class);
        
        $email = $request->email;
        $token = $request->token;
        try {
            Mail::to($email)->send(new Email($token));
            return Redirect::back()
              ->with(
                'success',
                'Email sent, user will be able to register when they are ready.'
              );
        } catch (\Exception $e) {
            echo 'Error - '.$e;
        }
    }

    //create invitation, send and assign user to study all at once
    public function assign(StoreInvitationRequest $request, Study $study)
    {
      
      $this->authorize('collabAdd', $study);
      
      $invitation = new Invitation();
      $invitation->email = $request->email;
      $invitation->generateInvitationToken();
      $invitation->assign_permission = "access$study->id";
      $invitation->save();

      Mail::to($invitation->email)->send( new Email($invitation->invitation_token) );
       return Redirect::back()
          ->with(
              'success', 
              'Invitation successfully sent. Please wait for user to register.'
            );
    }
}
