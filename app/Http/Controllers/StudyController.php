<?php

namespace App\Http\Controllers;

use Storage;
use App\Study;
use App\Basis;
use App\File;
use App\Jobs\ProcessBasisSimulation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

use Spatie\Permission\Models\Permission;

//searching package for searching
use Spatie\Searchable\Search;



class StudyController extends Controller
{
  
  public function __construct()
  {
    $this->middleware('auth');
  }
  
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    
      $this->authorize('list', Study::class);
      
      // get all the studies
      $studies = Study::all();
      $studies = $studies->reject( function ($study, $key) {
        return auth()->user()->cannot("access$study->id");
      });
      
      // load the view and pass the studies
      return view('studies.index')->with('studies',$studies);
  }

  /**
   * Create a new study
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      $this->authorize('create', Study::class);
      return view('studies.create');
  }


    /**
     * Create a new study
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function basis(Request $request)
    {
      
        $this->authorize('create', Study::class);
      
        $validatedData = $request->validate([
          'studyName' => 'bail|required|unique:studies,name|max:255',
          'studyType' => 'bail|required|max:255',
         ]);
         
        $studyName = $request->input('studyName');
        $studyType = $request->input('studyType');

        $study = new Study();
        $study->name = $studyName;
        $study->type = $studyType;
        $study->user_id = Auth::id();
        $study->save();
        
        //create permission to access study being created
        $permission = Permission::create(['name' => 'access' . $study->id]);
        //assign self to study you created
        auth()->user()->givePermissionTo($permission);
        
        // Store Study with default basis options
        if ($request->input('button') == 'create') {
            return redirect()->route('studies.index');
        }
        return view('studies.basis')->with(['study' => $study]);
    }

    /**
     * Store a new study
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function basisOptions(Request $request, Study $study)
    {

        $this->authorize('create', Study::class);
        
        $validator = Validator::make($request->all(), [
          'studyName' => 'bail|required|max:255',
          'studyType' => 'bail|required|max:255',
          'method' => 'nullable',
          'press' => 'nullable',
          'megapress' => 'nullable',
        ]);

        if ($validator->fails()) {
            return redirect('studies/create')
                        ->withErrors($validator)
                        ->withInput();
        }
         
        $studyName = $request->input('studyName');
        $studyType = $request->input('studyType');
        $press = ($request->input('press') == "on");
        $megapress = ($request->input('megapress') == "on");
        $method = $request->input('method');

        $study->name = $studyName;
        $study->type = $studyType;
        $study->user_id = Auth::id();
        $study->basisMethod = $method;

        //Log::debug("creating $study->name and its not null!");
        if ($press) {
            $pressBasis = new Basis();
            $pressBasis->seq = 'press';
            $pressBasis->study()->associate($study);
            $pressBasis->save();
        }

        if ($megapress) {
            $megapressBasis = new Basis();
            $megapressBasis->seq = 'megapress';
            $megapressBasis->study()->associate($study);
            $megapressBasis->save();
        }

        $study->save();

        Log::debug("Looping through basis");

        switch ($method) {
            case 'FID-A':
                return view('studies.basisFIDA')->with(['study' => $study]);
                break;
            case 'TARQUIN':
                return redirect()->route('studies.index');
                break;
            case 'Upload':
                return view('studies.basisUpload')->with(['study' => $study]);
                break;
        }
        return view('studies.create');
    }

    /**
     * Store a new study
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function basisFIDA(Request $request, Study $study)
    {
        $this->authorize('create', Study::class);

        $count = 0;
        foreach ($study->basis as $basis) {
            $samplingFrequency = $request->input('samplingFrequency');
            $echoTime = $request->input('echoTime');
            $basis->samplingFreq = $samplingFrequency[$count];
            $basis->echoTime = $echoTime[$count];

            log::debug('Generating new custom basis file');
            log::debug("echo time: $echoTime[$count]");
            log::debug("sampling frequency: $samplingFrequency[$count]");
            $basisFileName = str_random(40);
            Log::debug("file name: $basisFileName");
            $pathBasis = $basis->seq . 'Basis' . DIRECTORY_SEPARATOR . $basisFileName;
            Storage::put($pathBasis, "");
            Log::debug("file location: $pathBasis");
            $study->files()->save(new \App\File(['path' => $pathBasis, 'type' => 'basis']));
            $basis->path = $pathBasis;

            $basis->save();
            $study->save();
  
            ProcessBasisSimulation::dispatch($pathBasis, $study, $echoTime[$count], $samplingFrequency[$count]);

            $count += 1;
            Log::debug("Sampling Frequency: $basis->samplingFrequency" );
           // $basis->transmitterFrequency = $request->input($seq . 'transmitterFrequency');
        }

        return redirect()->route('studies.index');
    }

    /**
     * Create a new study with custom basis file
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     **/
    public function basisUpload(Request $request, Study $study)
    {
        $this->authorize('create', Study::class);
        return view('studies.basisUpload')->with(['study' => $study]);
    }

    /**
     * Store a new study
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Study $study)
    {
      
        $this->authorize('create', Study::class);

        if (!$request->has('basis')) {
            return view('studies.basisUpload')->with(['study' => $study]);
        }

        $basisSet = $request->file('basis');
        $count = 0;
        foreach ($study->basis as $basis) {
            
            $seq = $basis->seq;
            log::debug('Uploading press basis file');
            $fileName = str_random(40);
            $basisFileName = $fileName . '.basis';
            $pathBasis = Storage::putFileAs( $seq . 'Basis', $basisSet[$count], $basisFileName);
            $filePath = storage_path() . '/' . 'app/' . $pathBasis;
            $study->basis->path = $filePath;
            Log::debug("file location: $filePath");
            $study->files()->save(new \App\File(['path' => $pathBasis, 'type' => 'basis']));
            $study->save();
            $count++;
            
        }

        //create permission to access this session
        $permission = Permission::create(['name' => 'access' . $study->id]);
        //assign self to study you created
        auth()->user()->givePermissionTo($permission);

        return redirect()->route('studies.index');
    }

    //building search
    public function search(Request $request)
    {
        $this->authorize('list', Study::class);
        
        $validatedData = $request->validate([
           'searchStudyByName' => 'required',
         ]);
        
        // user search
        $studySearchResults = Study::where('name', 'like', "%{$request->searchStudyByName}%")
                                ->take(20)
                                ->get();
                  
        $studySearchResults = $studySearchResults->reject(function ($study, $key) {
            return !Auth::user()->can("access$study->id");
        });

        return view('studies.search', compact('studySearchResults'));
    }
    
    public function searchIndex(Request $request)
    {
        return view('studies.search');
    }
}
