<?php

namespace App\Http\Controllers;

use App\AnalysisResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AnalysisResultsController extends Controller
{
  public function __construct() {
    $this->middleware('auth');
  }

  public function display(String $id) {
    return $this->getFile($id);
  }
  
  public function download(String $id) {
    return $this->getFile($id);
  }
  
  protected function getFile($id) {
    $this->authorize('view',[AnalysisResult::class, $id]);
  
    $path = storage_path("app/$id");
    
    // return $path;
    if(file_exists($path)) {
      return response()->download($path);
    } else {
      exit('Requested file does not exist on our server!');
    }
  }
}
