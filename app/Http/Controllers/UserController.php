<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $this->authorize('list', User::class);
        
        // get all the users
        $users = User::all();
        // load the view and pass the users
        return view('systems.users.index')->with('users',$users);
    }

    /**
     * Show the profile of a user
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(User $user)
    {
        $this->authorize('view', $user);
      
        return view('systems.users.profile')->with('user',$user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', User::class);
        return view('systems.users.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', User::class);
      
        $firstName = $request->input('firstName');
        $lastName = $request->input('lastName');
        $email = $request->input('email');
        $title = $request->input('title');
        $facility = $request->input('facility');
        $phone = $request->input('phone');
        $password = $request->input('password','administrator');
    
        $validatedData = $this->validate($request,[
          'email' => 'required|email|unique:users,email|max:100',
          'firstName' => 'nullable|max:50',
          'lastName' => 'nullable|max:50',
          'title' => 'nullable|max:50',
          'facility' => 'nullable|max:50',
          'phone' => 'nullable|numeric',
        ]);

        $user = new User();
        
        //check admin checkbox
        if($request->get('admin')) {
            if(auth()->user()->hasrole('admin'))
            {
                $user->assignRole('admin');
            }
        }

        $user->firstName = $request->input('firstName');
        $user->lastName = $lastName;
        $user->email = $email;
        $user->title = $title;
        $user->facility = $facility;
        $user->phone = $phone;
        $user->password = Hash::make($password);
        $user->save();

        return redirect()->route('systems.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
      $this->authorize('update', $user);
    
      return view('systems.users.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, Request $request)
    {
      $this->authorize('update', $user);
      
      $firstName = $request->input('firstName');
      $lastName = $request->input('lastName');
      $email = $request->input('email');
      $title = $request->input('title');
      $facility = $request->input('facility');
      $phone = $request->input('phone');
      
      $validatedData = $this->validate($request,[
        'email' => "required|email|unique:users,email,$user->id|max:100",
        'firstName' => 'nullable|max:50',
        'lastName' => 'nullable|max:50',
        'title' => 'nullable|max:50',
        'facility' => 'nullable|max:50',
        'phone' => 'nullable|numeric',
      ]);
      
      $user->firstName = $firstName;
      $user->lastName = $lastName;
      $user->email = $email;
      $user->title = $title;
      $user->facility = $facility;
      $user->phone = $phone;
      $user->save();

      return redirect()->route('systems.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete', $user);
      
        //delete user
        $user->delete();
        return redirect()->route('systems.users.index');
    }
}
