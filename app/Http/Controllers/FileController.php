<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;

class FileController extends Controller
{
  public function __construct() {
    $this->middleware('auth');
  }
  
  public function display(String $id) {
    return $this->getFile($id);
  }
  
  public function download(String $id) {
    return $this->getFile($id);
  }
  
  public function getFile(String $id) {
    $this->authorize('view', [File::class, $id]);
    $path = storage_path("app/$id");

    if(file_exists($path)) {
      return response()->download($path);
    } else {
      exit('Requested file does not exist on our server!');
    }
  }
  
}
