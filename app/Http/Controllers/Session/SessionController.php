<?php

namespace App\Http\Controllers\Session;

use Storage;
use App\Session;
use App\User;
use App\Study;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Jobs\ProcessGEMegapress;
use App\Jobs\ProcessGEPress;
use App\Jobs\ProcessGEDicom;
use App\Jobs\ProcessAnalysisTarquin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
//permissions package for sharing
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;


class SessionController extends Controller
{

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index(Study $study)
    {   
      
        $this->authorize('view', [Session::class, $study]);
      
        $orderedSessions = $study->sessions()->orderBy('created_at', 'desc')->get();
        return view('sessions.index',['study'=>$study, 'sessions'=>$orderedSessions]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      
      //create a session under a study
      //no need  to give the study listings
      //check if user has access to study
      $this->authorize('create', [Session::class, $request->study]);
      
      // $studies = Auth::user()->studies();
      return view('sessions.session.create',['study'=>$request->study]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->authorize('create', [Session::class, $request->study]);
        
            $study = \App\Study::findOrFail($request->input('study'));
            
            //Create empty session
            $session = new Session();
            $session->study()->associate($study);
            $session->save();

            //Store megapress file and link with session
            //Stored Location in Storage/app/megapress/
            //Preprocessing by dispatching a job onto a queue
            if($request->has('megapress')) {
              $megapressFileName = str_random(40);
              $pathMegapress = Storage::putFileAs('megapress'.DIRECTORY_SEPARATOR.$megapressFileName, $request->file('megapress'), $megapressFileName.".7");
              $session->megapress_path = $pathMegapress;
              $session->files()->save(new \App\File(['path' => $pathMegapress, 'type' => 'megapress']));

            //Store voxel data and link with session
            //Stored Localation in Storage/app/voxel/
            //Processing by dispatching a job onto a queue
            if($request->has('dicoms')) {
                $dicomFileName = str_random(40);
                $pathDicom = Storage::putFileAs('dicoms'.DIRECTORY_SEPARATOR.$dicomFileName, $request->file('dicoms'), $dicomFileName . '.zip');
                $session->files()->save(new \App\File(['path' => $pathDicom, 'type' => 'dicoms']));
                ProcessGEDicom::dispatch($pathDicom, $pathMegapress, $session);
            }

            ProcessGEMegapress::withChain([
                new ProcessAnalysisTarquin($pathMegapress,$session,"megapress"),
                
                ])->dispatch($pathMegapress,$session,"megapress");

                
            }

            //Store press file and link with session
            //Stored Location in Storage/app/press/
            //Preprocessing by dispatching a job onto a queue
            if($request->has('press')) {
            $pressFileName = str_random(40);
            $pathPress = Storage::putFileAs('press'.DIRECTORY_SEPARATOR.$pressFileName, $request->file('press'), $pressFileName.".7");
            $session->press_path = $pathPress;
            $session->files()->save(new \App\File(['path' => $pathPress, 'type' => 'press']));
            // ProcessGEpress::dispatch($pathPress);
            ProcessGEpress::withChain([
                new ProcessAnalysisTarquin($pathPress,$session,"press")
            ])->dispatch($pathPress,$session,"press");
            }

            $session->save();
            return redirect()->route('sessions.show',['study'=>$study, 'session'=>$session]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\session  $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {   
        $this->authorize('view', [Session::class, $session->study]);
        return view('sessions.session.upload')->with('session',$session, 'files', $session->files);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\session  $session
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
      
      $this->authorize('update', $session);
      return view('sessions.edit');
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\session  $session
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Session $session)
    {
      $this->authorize('update', $session);
      $session->save();
      return redirect()->back();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\session  $session
     * @return \Illuminate\Http\Response
     */
    public function destroy(Session $session)
    {
      $this->authorize('delete', $session);

      $session->delete();
      return redirect()->route('sessions.index');
    }
}
