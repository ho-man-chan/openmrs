<?php

namespace App\Http\Controllers\Session;

use Storage;
use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class DicomController extends Controller
{
  
  public function __construct()
  {
    $this->middleware('auth');
  }
  
  /**
   * Display preprocess report of a session.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Session $session)
  {    

      $this->authorize('view', [Session::class, $session->study]);
      //get all preprocessed file by type
      $processedDicomFiles = $session->files()
        ->where(function($q) {
          $q->where('type', 'dicoms_output');
        })->get();

      return view('sessions.session.voxel')->with(['session'=>$session, 'processedDicomFiles'=>$processedDicomFiles]);
  }
}
