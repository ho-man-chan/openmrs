<?php

namespace App\Http\Controllers\Session;

use Storage;
use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class BrainBrowserController extends Controller
{
  
  public function __construct()
  {
    $this->middleware('auth');
  }
  
  /**
   * Display preprocess report of a session.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Session $session)
  {    
      $this->authorize('view', [Session::class, $session->study]);

      //get all preprocessed file by type
      $brainBrowserFiles = $session->files()
        ->where(function($q) {
          $q->where('type', 'nifti_output');
        })->get();
      
      $filteredArray = [];
      foreach($brainBrowserFiles as $key=>$bfile) {
        if(strpos(strtolower($bfile->name), 'megapress') !== false || strpos(strtolower($bfile->name), 'probe') !== false) {
        } else {
          array_push($filteredArray, $bfile);
        }
      };

      return view('sessions.session.brainbrowser')->with(['session'=>$session, 'brainBrowserFiles'=>$filteredArray]);
  }
}
