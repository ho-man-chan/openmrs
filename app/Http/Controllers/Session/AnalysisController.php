<?php

namespace App\Http\Controllers\Session;

use App\Analysis;
use App\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Jobs\ProcessAnalysisTarquin;
use Illuminate\Support\Str;

class AnalysisController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display table of all analyses
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Session $session)
  {
  
    $this->authorize('view', [Session::class, $session->study]);
    
    // get all the analyses
    $analyses = $session->analyses;
    // load the view and pass the analyses
    return view('sessions.session.analysis')->with(['session'=>$session, 'analyses'=>$analyses]);
  }

  /**
   * Show graph of analysis
   *
   * @return \Illuminate\Http\Response
   */
  public function show(Session $session, Analysis $analysis)
  {
    
    $this->authorize('view', [Session::class, $session->study]);
    
    //get all preprocessed file by type
    Log::debug("analysis id: $analysis->id");
    // Log::debug("session id: $session->id");
    $analyzedFiles = $analysis->results()
      ->where(function($q) {
        $q->where('type', 'megapress_graphs')
        ->orWhere('type','press_graphs');
      })->get();
    return view('sessions.session.analysis.show')->with(['session'=>$session, 'analyzedFiles'=>$analyzedFiles]);

  }

  /**
   * Create a new analysis
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Session $session)
  {
    
    $this->authorize('create', [Session::class, $session->study->id]);
    
      // // get default values
      // if($session->analyses->count() > 0) {
      
        $analysis = $session->analyses->sortByDesc('created_at')->first();
        $count = $session->analyses->count();
      // 
      // } else {
      //   // if default analysis is incomplete return 403 error
      //   return abort('403');
      // }
      
      return view('sessions.session.analysis.create')->with(['session'=>$session, 'analysis'=>$analysis]);
  }

  /**
   * Store a new analysis
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, Session $session)
  {
    
      $this->authorize('create', [Session::class, $session->study->id]);
      
      //Error reporting if no p-files is uploaded to session
      //The user will experience analysis not being created
      //Since no p-file can be analyzed.

      $analysisName = $request->input('analysisName',Str::random(6));
      $samplingFreq = $request->input('samplingFreq',0);
      $transmitterFreq = $request->input('transmitterFreq',0);
      $echoTime = $request->input('echoTime',0);
      $startPoint = $request->input('startPoint',0);
      $eddyCorrection = ($request->input('eddyCurrentCorrection') == "on");
      $stackPDF = ($request->input('stackPDF') == "on");
      $scanType = $request->input('scanType','press');
      Log::debug("stack pdf val: $stackPDF");
      Log::debug("eddy val: $eddyCorrection");
      
      $analysis = new Analysis();

      $analysis->name = $analysisName;
      $analysis->session_id = $session->id;
      $analysis->default = false;
      $analysis->sampling_freq = $samplingFreq;
      $analysis->transmitter_freq = $transmitterFreq;
      $analysis->echo_time = $echoTime;
      $analysis->start_point = $startPoint;
      $analysis->stack_pdf = $stackPDF;
      $analysis->eddy_correction = $eddyCorrection;

      Log::debug("scanType: $scanType");
      if (!is_null($session->press_path) and $scanType == "Press" or $scanType == "All Scans") {
        // run press analysis
        $press_analysis = $analysis->replicate();
        $press_analysis->type = "press";
        $press_analysis->save();
        ProcessAnalysisTarquin::dispatch($session->press_path,$session,"press");
      }

      if (!is_null($session->megapress_path) and $scanType == "Mega-Press" or $scanType == "All Scans") {
        // run megapress analysis
        $megapress_analysis = $analysis->replicate();
        $megapress_analysis->type = "mega_press";
        $megapress_analysis->save();
        ProcessAnalysisTarquin::dispatch($session->megapress_path,$session,"megapress");
      }

      // get all the analyses
      $analyses = $session->analyses;
      // load the view and pass the analyses
      return view('sessions.session.analysis')->with(['session'=>$session, 'analyses'=>$analyses]);
  }


}
