<?php

namespace App\Http\Controllers\Session;

use Storage;
use App\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PreprocessController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display preprocess report of a session.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Session $session)
  {    
    
      $this->authorize('view', [Session::class, $session->study]);
        
      //get all preprocessed file by type
      $preprocessedFiles = $session->files()
        ->where(function($q) {
          $q->where('type', 'megapress_preprocessed_report_pdf')
          ->orWhere('type','press_preprocessed_report_pdf');
        })->get();

      return view('sessions.session.preprocess')->with(['session'=>$session, 'preprocessedFiles'=>$preprocessedFiles]);
  }
}
