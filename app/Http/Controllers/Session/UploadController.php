<?php

namespace App\Http\Controllers\Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Session;

class UploadController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Session $session)
  {
      $this->authorize('view', [Session::class, $session->study]);
      return view('sessions.session.upload')->with('session',$session);
  }

}
