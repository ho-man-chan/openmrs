<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    public function handle($request, Closure $next)
    {
        if (!auth()->check() || !auth()->user()->hasrole('admin')) {
            return redirect(route('home'));
        }

        return $next($request);
    }
}
