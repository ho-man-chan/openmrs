<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Study extends Model implements Searchable
{
  
  protected $fillable = ['name'];
  
  public function user()
  {
    return $this->belongsTo('App\User');
  }
  
  public function sessions() {
    return $this->hasMany('App\Session');
  }

  public function basis() {
    return $this->hasMany('App\Basis');
  }

  public function files()
  {
     return $this->hasMany('App\File');
     // return $this->morphMany('App/File','filable');
  }
  
  public function getSearchResult(): SearchResult
  {
    $url = route('studies.show', $this->id);

      return new SearchResult(
        $this,
        $this->name,
        $url
      );
  }
}
