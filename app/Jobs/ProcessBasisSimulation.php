<?php

namespace App\Jobs;

use App\Study;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use PDF;

class ProcessBasisSimulation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $timeout = 600;
    protected $pathBasis;
    protected $study;
    protected $echoTime;
    protected $samplingFrequency;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $pathBasis, Study $study, String $echoTime, String $samplingFreq)
    {
        $this->pathBasis = $pathBasis;
        $this->study = $study;
        $this->echoTime = $echoTime;
        $this->samplingFrequency = $samplingFreq;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        log::debug("Simulating Basis Set....");
        //update basis status to basis simulation
        // $file = \App\File::where('path', $this->pathBasis)->first();
        // $file->status = "simulating basis set";
        // $file->save();

        //build paths for basis simulation
        $pathFull = storage_path() . '/' . 'app/' . $this->pathBasis;
        $pathWithoutFile = implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, $pathFull), 0, -1));
        $filePathRelativeArray = explode(DIRECTORY_SEPARATOR, $this->pathBasis);
        $folderPath = implode(DIRECTORY_SEPARATOR, array_slice($filePathRelativeArray, 0, -1));
        $pathBasis = end($filePathRelativeArray);
        $fidaPath = env('FIDA_PATH', 'N/A');
        $matlabPath = env('MATLAB_PATH', '');

        // MATLAB simulation script execution
        $matlabCommand = "matlab -nodisplay -nosplash -sd \"$pathWithoutFile\" -r \"addpath(genpath(['$fidaPath'])); sim_lcm_press_basis('$pathFull', $this->echoTime, $this->samplingFrequency); exit;\"";

        $commands = $matlabPath . $matlabCommand;
        log::debug("Shell Command: ");
        log::debug($commands);

        // execute command
        $cmdOutput = shell_exec($commands);

        //update session file progress to preprocessed
        // $file = \App\File::where('path', $this->pathBasis)->first();
        // $file->status = "simulation complete";
        // $file->save();
        log::debug("Basis Simulation Complete");

        $filepath = "$folderPath".DIRECTORY_SEPARATOR."$pathBasis.basis";
        Log::debug("file path: $filepath");
        //link lcm to session
        $fileBasis = new \App\File;
        $fileBasis->type = "basis";
        $fileBasis->path = $filepath;
        $fileBasis->study_id = $this->study->id;
        $fileBasis->save();
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed()
    {
        Log::error("Fail preprocessing: $this->pathBasis");
    }
}
