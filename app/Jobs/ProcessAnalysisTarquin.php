<?php

namespace App\Jobs;

use App\Session;
use App\Study;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Events\SessionProgressStatusEvent;


class ProcessAnalysisTarquin implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  public $tries = 5;
  public $timeout = 180;
  protected $path;
  protected $session_id;
  protected $session;
  protected $samplingFreq;
  protected $transmitterFreq;
  protected $echoTime;
  protected $startPoint;
  protected $isDefault;
  protected $analysis;
  protected $eddyCorrection;
  protected $stackPDF;
  protected $type;
  public $extension;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct(String $path, Session $session, String $type)
  {

    $this->path = $path;
    $this->session = $session;
    $this->session_id = $session->id;

    Log::debug("process tarquin: sessionid: $session->id");
    $this->type = $type;
    Log::debug("type: $this->type");
    if ($this->type == "press") {
      $this->extension = "_lcm";
    } else {
      $this->type = "mega_press";
      $this->extension = "_diff_lcm";
    }
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {

    Log::debug("Session #: $this->session_id | Analyzing with TARQUIN...");


    //build paths for preprocessing
    $pathFull = storage_path() . '/' . 'app/' . $this->path;
    $pathWithoutFile = implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, $pathFull), 0, -1));
    $filePathRelativeArray = explode(DIRECTORY_SEPARATOR, $this->path);
    $folderPath = implode(DIRECTORY_SEPARATOR, array_slice($filePathRelativeArray, 0, -1));
    $fileName = end($filePathRelativeArray);
    $tarquinPath = env('TARQUIN_PATH');
    $type = $filePathRelativeArray[0];

    //update megapress status to analyzed
    $lcmws = "$folderPath" . DIRECTORY_SEPARATOR . $fileName . $this->extension;
    //Log::debug($lcmws);
    // $filelcmws = \App\File::where('path', $lcmws)->first();
    // $filelcmws->status = "analyzing";
    // $filelcmws->save();
    //
    // $lcmw = "$folderPath".DIRECTORY_SEPARATOR.$fileName."_w_lcm";
    // $filelcmw = \App\File::where('path', $lcmw)->first();
    // $filelcmw->status = "analyzing";
    // $filelcmw->save();

    $lcmwsFullPath  = "$pathWithoutFile" . DIRECTORY_SEPARATOR . "$fileName" . $this->extension;

    $lcmwFullPath  = "$pathWithoutFile" . DIRECTORY_SEPARATOR . "$fileName" . "_w_lcm";

    $lcmwText = file_get_contents($lcmwFullPath);
    $lcmwsText = file_get_contents($lcmwsFullPath);

    // Set frequency values

    // Check if analyses is not empty
    if ($this->session->analyses->count() > 0) {
      // get latest analysis
      $this->analysis = $this->session->analyses->sortByDesc('created_at')->first();
      $this->isDefault = $this->analysis->default;
      //Log::debug("isDefault: $isDefault");
      if (!$this->isDefault) {
        Log::debug("creating a new analysis");
        $this->analysis->type = $this->type;
        $this->analysis->save();
        $this->samplingFreq = $this->analysis->sampling_freq;
        $this->transmitterFreq = $this->analysis->transmitter_freq;
        $this->echoTime = $this->analysis->echo_time;
        $this->startPoint = $this->analysis->start_point;
        $this->stackPDF = ($this->analysis->stack_pdf) ? "true" : "false";
        $this->eddyCorrection = ($this->analysis->eddy_correction) ? "true" : "false";
        Log::debug("stackPDF: $this->stackPDF");
      } else {
        $this->getDefaultAnalysis($lcmwText);
      }
    } else {
      // else get values from header
      $this->isDefault = true;
      $this->getDefaultAnalysis($lcmwText);
    }

    // Log::debug("FS: $this->samplingFreq");
    // Log::debug("FT: $this->transmitterFreq");

    // set analysis output path
    $textOutputPath = $pathWithoutFile . "/results_" . $this->analysis->type . "_" . $this->analysis->id . ".txt";
    $graphOutputPath = $pathWithoutFile . "/graph_" . $this->analysis->type . "_" . $this->analysis->id . ".pdf";

    // Execute TARQUIN

    // default command 
    Log::debug("Default: $this->isDefault");

    $study = $this->session->study;
    Log::debug("Study Name: $study->name");

    if( $study->basis == "FID-A" || $study->basis == "Other") {
      $this->samplingFreq = $study->sampling_frequency;
      $this->transmitterFreq = $study->transmitter_frequency; 
    }

    // TARQUIN command line 
    $tarquinCommand = "./tarquin --input $lcmwsFullPath --input_w $lcmwFullPath --fs $this->samplingFreq --ft $this->transmitterFreq --format lcm --pul_seq $this->type --output_txt $textOutputPath --output_pdf $graphOutputPath --ext_pdf $this->stackPDF --stack_pdf $this->stackPDF --water_eddy $this->eddyCorrection";

    // add additional parameters

    if ($this->isDefault) {
      $tarquinCommand = $tarquinCommand . "--start_pnt $this->startPoint";
    }
    // check if study->basis is TARQUIN or FID-A
    // $study = Post::whereHas('id', function ($query) {
    //   $query->where('id', '==', $this->session->study_id);
    // })->get();

    // Append basis path
    $basisPath = $study->basis_path;
    
    if( $study->basis == "FID-A" || $study->basis == "Other") {
      $tarquinCommand = $tarquinCommand . " --basis_lcm " . $basisPath;
    }

    $commands = $tarquinPath . $tarquinCommand;

    Log::debug($commands);

    // execute command
    $cmdOutput = shell_exec($commands);

    // event(new SessionProgressStatusEvent($file->session_id));

    log::debug($cmdOutput);

    //update session file progress to preprocessed
    // $filelcmws = \App\File::where('path', $lcmws)->first();
    // $filelcmws->status = "analyzed";
    // $filelcmws->save();
    //
    // $filelcmw = \App\File::where('path', $lcmw)->first();
    // $filelcmw->status = "analyzed";
    // $filelcmw->save();

    // CHANGE this to link to analysis
    //link results to session
    $analysisResult = new \App\AnalysisResult;
    $analysisResult->type = "$type" . "_results";
    $analysisResult->path = "$folderPath" . DIRECTORY_SEPARATOR . "results_" . $this->analysis->type . "_" . $this->analysis->id . ".txt";
    $analysisResult->analysis_id = $this->analysis->id;
    $analysisResult->save();

    //link results to session
    $analysisGraph = new \App\AnalysisResult;
    $analysisGraph->type = "$type" . "_graphs";
    $analysisGraph->path = "$folderPath" . DIRECTORY_SEPARATOR . "graph_" . $this->analysis->type . "_" . $this->analysis->id . ".pdf";
    $analysisGraph->analysis_id = $this->analysis->id;
    $analysisGraph->save();
    
    //update session progress
    event(new SessionProgressStatusEvent($this->session->id));

    Log::debug("Session #: $this->session_id | Analysis completed successfully!");

    /* Working CSV conversion, Commented out until use required.
      // convert results.txt to csv
      $results_path = "$pathWithoutFile".DIRECTORY_SEPARATOR."results.txt";
      $resultsText = file_get_contents($results_path);

      $metabolitesName = array(); // name of metabolite
      $metabolitesConcentration = array(); // concentrations
      $metabolitesPercentSD = array(); // percent standard deviation
      $metabolitesSD = array(); // standard deviation

      $resultsRows = explode("\n", $resultsText);

      for ($i = 0; $i <= 33; $i++ ) {
        $metaboliteData = preg_split('/\s+/', $resultsRows[$i+2], -1, PREG_SPLIT_NO_EMPTY); // parse metabolite values
        $metabolitesName[$i] = $metaboliteData[0];
        //Log::debug("metabolite name: $metabolitesName[$i]");
        $metabolitesConcentration[$i] = $metaboliteData[1];
        $metabolitesPercentSD[$i] = $metaboliteData[2];
        $metabolitesSD[$i] = $metaboliteData[3];
      }

      $metabolitesName_csv = fopen("$pathWithoutFile".DIRECTORY_SEPARATOR."metabolitesName.csv", 'w');
      $metabolitesConcentration_csv = fopen("$pathWithoutFile".DIRECTORY_SEPARATOR."metabolitesConcentration.csv", 'w');
      $metabolitesPercentSD_csv = fopen("$pathWithoutFile".DIRECTORY_SEPARATOR."metabolitesPercentSD.csv", 'w');
      $metabolitesSD_csv = fopen("$pathWithoutFile".DIRECTORY_SEPARATOR."metabolitesSD.csv", 'w');

      $this->toCSV($metabolitesName_csv, $metabolitesName);
      $this->toCSV($metabolitesConcentration_csv, $metabolitesConcentration);
      $this->toCSV($metabolitesPercentSD_csv, $metabolitesPercentSD);
      $this->toCSV($metabolitesSD_csv, $metabolitesSD);
      */
  }

  /*
    * parses frequencies from the LCM file
    * creates new analysis with those values
    */
  public function getDefaultAnalysis($lcmwText)
  {
    $rows = explode("\n", $lcmwText);

    $hzpppm = $rows[3];
    $dwellTime = $rows[5];
    $echoTime = $rows[1];

    $hzpppmSplit = explode(' ', $hzpppm);
    $dwellTimeSplit = explode(' ', $dwellTime);
    $echoTimeSplit = explode(' ', $echoTime);

    $this->transmitterFreq = $hzpppmSplit[2] . 'E6';
    $this->echoTime = $echoTimeSplit[2];
    $dwellTimeValue = $dwellTimeSplit[2];
    $this->samplingFreq = (string)(1 / $dwellTimeValue);

    // create new Analysis
    $analysis = new \App\Analysis;
    $analysis->name = 'Default ' . $this->type . ' Analysis';
    $analysis->type = $this->type;
    $analysis->session_id = $this->session_id;
    $analysis->default = true;
    $analysis->sampling_freq = $this->samplingFreq;
    $analysis->transmitter_freq = $this->transmitterFreq;
    $analysis->echo_time = $this->echoTime;
    $analysis->stack_pdf = true;
    $analysis->eddy_correction = true;
    $analysis->save();

    $this->analysis = $analysis;
    $this->startPoint = $analysis->start_point;
    $this->stackPDF = "true";
    $this->eddyCorrection = "true";
  }

  /**
   * converts an array to csv
   * $file is the write file stream
   * $data is the array
   * @return void
   */
  public function toCSV($file, $data)
  {
    fputcsv($file, $data);
    fclose($file);
  }
}
