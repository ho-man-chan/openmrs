<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use PDF;
use App\Events\SessionProgressStatusEvent;


class ProcessGEMegapress implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $timeout = 180;
    protected $pathMegapress;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $pathMegapress)
    {
        $this->pathMegapress = $pathMegapress;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      //update megapress status to preprocessing
      $file = \App\File::where('path', $this->pathMegapress)->first();
      $file->status = "preprocessing";
      $file->save();
      
      Log::debug("Session #: $file->session_id | Preprocessing MegaPress scan with FID-A...");

      event(new SessionProgressStatusEvent($file->session_id));

      //build paths for preprocessing
      $pathFull = storage_path() .'/'. 'app/' . $this->pathMegapress;
      $pathWithoutFile = implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, $pathFull), 0, -1));
      $filePathRelativeArray = explode(DIRECTORY_SEPARATOR,$this->pathMegapress);
      $folderPath = implode(DIRECTORY_SEPARATOR, array_slice($filePathRelativeArray, 0, -1));
      $fileName = end($filePathRelativeArray);
      $fidaPath = env('FIDA_PATH','N/A');
      $matlabPath = env('MATLAB_PATH','');

      $matlabCommand = "matlab -nodisplay -nosplash -sd \"$pathWithoutFile\" -r \"addpath(genpath(['$fidaPath']));[out, outw] = run_megapressproc_GEauto(['$fileName']); exit;\"";

      $commands = $matlabPath . $matlabCommand;

      // log::debug($commands);

      // execute command
      $cmdOutput = shell_exec($commands);

      // log::debug($cmdOutput);

      //update session file progress to preprocessed
      $file = \App\File::where('path', $this->pathMegapress)->first();
      $file->status = "preprocessed";
      $file->save();
      
      event(new SessionProgressStatusEvent($file->session_id));

      //link lcm to session
      $filelcm = new \App\File;
      $filelcm->type = "megapress_lcm";
      $filelcm->path = "$folderPath".DIRECTORY_SEPARATOR.$fileName."_w_lcm";
      $filelcm->session_id = $file->session_id;
      $filelcm->save();
      
      event(new SessionProgressStatusEvent($file->session_id));

      //convert html to pdf and save
      $html_path = "$pathWithoutFile/report/report.html";
      $html_output_path = "$pathWithoutFile/report/report.pdf";

      $html = file_get_contents($html_path);

      //remove redundant space, newline, return
      //to generate pdf properly
      $html = str_replace('src= " ', 'src="', $html);
      $html = str_replace('.jpg "', '.jpg"', $html);
      $html = str_replace("\n", "", $html); $html = str_replace("\r", "", $html);

      //generate pdf
      $pdf = PDF::loadHtml($html);
      $pdf->save($html_output_path);

      $filelcm = new \App\File;
      $filelcm->type = "megapress_preprocessed_report_pdf";
      $filelcm->path = "$folderPath/report/report.pdf";
      $filelcm->session_id = $file->session_id;
      $filelcm->save();
      event(new SessionProgressStatusEvent($file->session_id));
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed()
    {
        
        Log::error("Fail preprocessing: $this->pathMegapress");
    }
}
