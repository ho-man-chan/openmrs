<?php

namespace App\Jobs;

use Storage;
use App\Session;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Chumper\Zipper\Zipper;
use App\Events\SessionProgressStatusEvent;

class ProcessGEDicom implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;
    public $timeout = 300;
    protected $session_id;
    protected $pathDicom;
    protected $pathMegapress;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $pathDicom, String $pathMegapress, Session $session)
    {
        $this->session_id = $session->id;
        $this->pathDicom = $pathDicom;
        $this->pathMegapress = $pathMegapress;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      //build path for megapress
      $file = \App\File::where('path', $this->pathMegapress)->first();
      $pathMegapressFull = storage_path() .'/'. 'app/' . $this->pathMegapress;

      //build paths for preprocessing
      $dfile = \App\File::where('path', $this->pathDicom)->first();
      $dfile->status = "analyzing";
      $dfile->save();
      
      event(new SessionProgressStatusEvent($dfile->session_id));
      
      $pathFull = storage_path() .'/'. 'app/' . $this->pathDicom;
      $pathWithoutFile = implode(DIRECTORY_SEPARATOR, array_slice(explode(DIRECTORY_SEPARATOR, $pathFull), 0, -1));

      //Extract dicom zip into folder
      $zipper = new Zipper();
      $zipper->make($pathFull)->extractTo($pathWithoutFile);
      
      //build dicom folder
      $extractedFolder = $pathWithoutFile . '/A';
      
      $filePathRelativeArray = explode(DIRECTORY_SEPARATOR,$this->pathDicom);
      $folderPath = implode(DIRECTORY_SEPARATOR, array_slice($filePathRelativeArray, 0, -1));
      
      //renames file to random string
      foreach(glob($extractedFolder.'/*') as &$file) {
          $dicomArray = explode('/', $file);
          Storage::move($folderPath.'/A'.'/'.end($dicomArray), $folderPath.'/A'.'/'.str_random(40));
      }
      
      $fileName = end($filePathRelativeArray);
      $spmPath = env('SPM_PATH','');
      $gannetPath = env('GANNET_PATH','');
      $matlabPath = env('MATLAB_PATH','');

      $niftiConversionCommand = "dcm2niix \"$extractedFolder\"";

      $execCommand = shell_exec($niftiConversionCommand);

      Storage::makeDirectory($folderPath . '/Nifti');
      foreach(glob($extractedFolder.'/*.nii') as &$file) {
        $niftiFile = explode('/', $file);
        Log::alert($niftiFile);
        Storage::move($folderPath.'/A'.'/'.end($niftiFile), $folderPath.'/Nifti'.'/'.end($niftiFile));

        // link nifti to session
        $nfile_output = new \App\File;
        $nfile_output->type = 'nifti_output';
        $nfile_output->name = end($niftiFile);
        $nfile_output->path = $folderPath.'/Nifti'.'/'.end($niftiFile);
        $nfile_output->session_id = $this->session_id;
        $nfile_output->status = 'generated';
        $nfile_output->save();
      }

      Storage::makeDirectory($folderPath . '/Json');
      foreach(glob($extractedFolder.'/*.json') as &$file) {
        $jsonFile = explode('/', $file);
        Log::alert($jsonFile);
        Storage::move($folderPath.'/A'.'/'.end($jsonFile), $folderPath.'/Json'.'/'.end($jsonFile));
      }

      $matlabCommand = "matlab -nodisplay -nosplash -sd \"$pathWithoutFile\" -r \"addpath(genpath(['$spmPath']));addpath(genpath(['$gannetPath']));MRS=GannetLoad({'$pathMegapressFull'});MRS=GannetCoRegister(MRS,{'$extractedFolder'}); exit;\"";

      // Log::debug("processGEDicom matlab cmd: $matlabCommand");
      $commands = $matlabPath . $matlabCommand;
    //   $commands = $matlabCommand;

      // execute command
      $cmdOutput = shell_exec($commands);

      // // link nifti to session
      // $nfile_output = new \App\File;
      // $nfile_output->type = 'nifti_output';
      // $nfile_output->path = $folderPath.'/Nifti';
      // $nfile_output->session_id = $this->session_id;
      // $nfile_output->status = 'generated';
      // $nfile_output->save();

      //update session file progress to preprocessed
      $file = \App\File::where('path', $this->pathDicom)->first();
      $file->status = "preprocessed";
      $file->save();
      
      event(new SessionProgressStatusEvent($file->session_id));


      //link dicom to session
      $file_output = new \App\File;
      $file_output->type = 'dicoms_output';
      $fullPathToCoRegister = glob($pathWithoutFile.'/GannetCoRegister_output/*')[0];
      $fullPathToCoRegisterArray = explode(DIRECTORY_SEPARATOR, $fullPathToCoRegister);
      $dicoms_output_file = end($fullPathToCoRegisterArray);
      $file_output->path = $folderPath.'/GannetCoRegister_output'.'/'.$dicoms_output_file;
      $file_output->session_id = $this->session_id;
      $file_output->status = 'analyzed';
      $file_output->save();
      
      event(new SessionProgressStatusEvent($file_output->session_id));
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed()
    {
        Log::error("Fail preprocessing: $this->pathDicom");
    }
    
}
