<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Basis extends Model 
{
  public $table = "basis";

  public function study()
  {
      return $this->belongsTo('App\Study');
  }

}


