<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Session;
use Illuminate\Support\Facades\Log;

class SessionProgressStatusEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $sessionId;
    
    public $broadcastQueue = 'high';
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($sessionId=22)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        Log::debug('session.'.$this->sessionId);
        return new PrivateChannel('session.'.$this->sessionId);
    }
    
    public function broadcastAs() {
        return "session-update";
    }
    
    public function broadcastWith()
    {
      $session = Session::where('id',$this->sessionId)->with('files')->get();
      return [
        'session' => $session
      ];
    }
}
