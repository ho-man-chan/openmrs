<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analysis extends Model
{
  public function session()
  {
      return $this->belongsTo('App\Session');
  }

  public function results() {
    return $this->hasMany('App\AnalysisResult');
  }
}
