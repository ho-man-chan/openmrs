# OpenMRS

An open source web-based Magnetic Resonance Spectroscopy Analyzer.

## Installation

0. Navigate to project folder.

1. Create a file named '.env' (environment file) in the project folder by using the file '.env.example' as the template. (Simply copy and paste the '.env.example' file to the main directory and name it '.env'.)
Edit the .env with the proper access credentials.

By default the admin user is defined in the environment file under

```bash
USER_FIRSTNAME=admin
USER_LASTNAME=openmrs
USER_USERNAME=admin
USER_EMAIL=admin@opemmrs.com
USER_PASS=openMrs123!
````

so change it to something more secure...

2. Inserting Matlab and FID-A details in environment file
Set the following to:
QUEUE_CONNECTION=database

MATLAB_PATH=MATLAB_EXECUTABLE_FOLDER_PATH
ex: MATLAB_PATH=
May leave empty if matlab command works

PROJECT_PATH=OPENMRS_FOLDER_PATH
ex: PROJECT_PATH=/home/vagrant/projects/openmrs

FIDA_PATH=FIDA_FOLDER_PATH
ex: FIDA_PATH=/home/vagrant/projects/FID-A

TARQUIN_PATH=TARQUIN_FOLDER_PATH
ex: TARQUIN_PATH=/home/vagrant/projects/TARQUIN_Linux_4.3.11_RC/

GANNET_PATH=GANNET_FOLDER_PATH
ex: GANNET_PATH=/home/vagrant/projects/gannet

SPM_PATH=SPM_FOLDER_PATH
ex: SPM_PATH=/home/vagrant/projects/spm12

3. Setup mailgun as the SMTP Provider
In your .env file, you have to fill in the following:

````bash
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailgun.org
MAIL_PORT=587
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=
MAIL_FROM_NAME=
````

3. Setup pusher as the broadcaster. Create a pusher account and create a new app called openmrs.
In your .env file, you have to fill in the following. See Channel app for credentials:

````bash
PUSHER_APP_ID=*******
PUSHER_APP_KEY=*******
PUSHER_APP_SECRET=*******
PUSHER_APP_CLUSTER=*******
BROADCAST_DRIVER=pusher
````

4. In the project folder, execute the following commands:

```bash
php artisan migrate:refresh --seed
```

5. Install js components

```bash
npm install
```

## Usage
Launch application by running the following commands in the openmrs main folder
Open two command prompt.

````bash
php artisan queue:listen --queue=default --timeout=300 & php artisan queue:listen --queue=high --timeout=180
````

````bash
npm run watch
````

## Testing

1. For browser tesing run: 

````bash
php artisan dusk
````

### Environment Handling
You can create an seperate environment for testing. For this, create a '.env.dusk.local' in your parent repository (same level as the .env). This environment file can be the same as the env, but change the DB_DATABASE to another value so it wouldn't override anything during testing. Of course, you would need to make a seperate database.
https://laravel.com/docs/5.8/dusk#environment-handling

If you have any problems, check the Laravel documentation on Dusk Installation: https://laravel.com/docs/5.8/dusk#installation

## Common Errors
1. If the running in development mode complains about missing autoload.php in ./vendor folder, run the following in your laravel project folder on the terminal:

````bash
composer update --no-scripts
````

2. If the url to laravel project complains about not having an application encryption key, run the following in your laravel project folder on the terminal:

````bash
php artisan key:generate
````
## Developers
To retrieve a secure file, provide the path of the file ex:

```bash
{{route('file.download',['id'=>$file->path])}}
```

## Gannet
In order to have Gannet working, you'll need two github repo: Gannet3.0 and SPM12

1. Clone the repo and place it to your homestead folder.
2. Include path to both Gannet3.0 and SPM12 to your .env file
3. For now, rename the dicom folder to A and zip it
4. To have the Gannet run, you need to provide megapress p file and the dicom folder, as Jamie mentioned, we don't typically use press for voxel location

## DCM2NIIX
In order to have the dicom converted to nifti file, dcm2niix needs to be installed from the command line in the homestead server using the listed command line depending on which OS you are using.

MacOS: brew install dcm2niix
Linux: sudo apt-get install dcm2niix

If the use Conda, this command line applies to MacOS, Linux and Windows.

Conda: conda install -c conda-forge dcm2niix

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
